菜单配置
=========

> 配置编辑器的菜单和右键菜单

文件位置：`~/.kde/share/apps/katepart/katepartui.rc`

#### 文件结构

```xml
<!DOCTYPE kpartgui SYSTEM 'kpartgui.dtd'>
<kpartgui version="60" name="KatePartView">
 <MenuBar>
  <Menu noMerge="1" name="view">
   <text>&amp;View</text>
   <Action group="view_operations" name="switch_to_cmd_line"/>
   <Separator group="view_operations"/>
   <Menu group="view_operations" name="codefolding">
    <text>&amp;Code Folding</text>
    <Action group="view_operations" name="folding_collapselocal"/>
    <Separator group="view_operations"/>
    <Action group="view_operations" name="folding_collapse_dsComment"/>
   </Menu>
   <Action group="view_operations" name="view_inc_font_sizes"/>
   <Action group="view_operations" name="view_dec_font_sizes"/>
  </Menu>
 <Menu noMerge="0" name="ktexteditor_popup">
  <Action group="popup_operations" name="edit_cut"/>
  <Action group="popup_operations" name="edit_copy"/>
  <Action group="popup_operations" name="edit_paste"/>
  <Separator group="popup_operations"/>
  <Action group="popup_operations" name="edit_select_all"/>
  <Action group="popup_operations" name="edit_deselect"/>
  <Separator group="popup_operations"/>
  <Action group="popup_operations" name="spelling_suggestions"/>
  <Separator group="popup_operations"/>
  <Action group="popup_operations" name="bookmarks"/>
  <Separator group="popup_operations"/>
 </Menu>
 <ToolBar noMerge="1" name="mainToolBar">
  <text>Main Toolbar</text>
  <Action group="file_operations" name="file_save"/>
 </ToolBar>
 <ActionProperties scheme="Default"/>
</kpartgui>
```


语法高亮配置
============

> `/usr/share/kde4/apps/katepart/syntax/`

snippets数据文件
=========

> phpsnippets  `~/.kde/share/apps/ktexteditor_snippets/ghns`

复制到目录以后，到配置中勾选开启


构建插件
==========

`php -c "/opt/php-5.3.21/etc/php.ini"  /home/snowair/CVS/bitbucket/markdown-ztree/markdown.php  -f  "%f"`