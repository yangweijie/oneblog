 
## session组件

> 该组件主要用来配置默认的session模式，实现自定义机制的基类。非登录状态下操作session时，建议按原生的PHP方式操作。如果是登录状态后的信息，建议通过user组件操作。

### 该组件提供了三种实现：

### 1. CHttpSession > _CApplicationComponent,IteratorAggregate, ArrayAccess, Countable_

```php
<?php
  ....
  $session=new CHttpSession;
  $session->open();
  $value1=$session['name1'];  // get session variable 'name1'
  $value2=$session['name2'];  // get session variable 'name2'
  $session['name3']=$value3;  // set session variable 'name3'
  foreach($session as $name=>$value) // traverse all session variables
```


<table width="100%" style='border-collapse: collapse;width:auto;' border='1' bordercolor='#ddd' cellpadding='6'>


	<tbody>
	<tr>
		<td style="" ><strong>配置属性</strong></td>
		<td style="" ><strong>说明</strong></td>
	</tr>

	<tr>
		<td style="" >$autoStart</td>
		<td style="" >默认值true</td>
	</tr>

	<tr>
		<td style="" >$cookieMode</td>
		<td style="" >'none', 'allow' and 'only'</td>
	</tr>

	<tr>
		<td style="" >$cookieParams</td>
		<td style="" >session的cookie参数配置，使用数组</td>
	</tr>

	<tr>
		<td style="" >$gCProbability</td>
		<td style="" >gc频率百分比默认为1%，使用浮点数设置</td>
	</tr>

	<tr>
		<td style="" >$savePath</td>
		<td style="" >默认存储路径为/tmp</td>
	</tr>

	<tr>
		<td style="" >$sessionName</td>
		<td style="" >默认使用php.ini中的配置</td>
	</tr>

	<tr>
		<td style="" >$timeout</td>
		<td style="" >session过期时间,默认值1440</td>
	</tr>

	<tr>
		<td style="" >$useCustomStorage</td>
		<td style="" >是否使用自定义机制，CHttpSession总是返回false。需使用该类的子类实现自定义机制</td>
	</tr>

	<tr>
		<td style="" >$useTransparentSessionID</td>
		<td style="" >是否使用GET方式传递sid，默认为false</td>
	</tr>

	<tr>
		<td style="" ><strong>读取属性</strong></td>
		<td style="" ><strong>说明</strong></td>
	</tr>

	<tr>
		<td style="" >$sessionID</td>
		<td style="" >当前用户的sid</td>
	</tr>

	<tr>
		<td style="" >$isStarted</td>
		<td style="" >是否已经开始了session进程</td>
	</tr>

	<tr>
		<td style="" >$iterator</td>
		<td style="" >session数据的CHttpSessionIterato迭代器实例</td>
	</tr>

	<tr>
		<td style="" >$count</td>
		<td style="" >session变量的数量</td>
	</tr>

	<tr>
		<td style="" >$keys</td>
		<td style="" >session变量列表</td>
	</tr>

	<tr>
		<td style="" ><strong>方法</strong></td>
		<td style="" ><strong>说明</strong></td>
	</tr>

	<tr>
		<td style="" >open</td>
		<td style="" ></td>
	</tr>

	<tr>
		<td style="" >close()</td>
		<td style="" ></td>
	</tr>

	<tr>
		<td style="" >destroy()</td>
		<td style="" ></td>
	</tr>

	</tbody>
</table>



### 2. CCacheHttpSession > CHttpSession

新增配置属性：cacheID,默认值为'cache'


### 3. CDbHttpSession > CHttpSession

> 主要用来保证集群应用下，session数据的跨服务器有效性

#### 新增属性：

* $connectionID

	数据库组件ID,如果没有配置，自动创建一个sqlite的db数据库文件，位置在`protected/runtime/session-YiiVersion.db`

* $autoCreateSessionTable=true

	是否自动创建需要的数据表，默认为true

* $sessionTableName='YiiSession';

	数据表名称,默认为：YiiSession
	
## user 组件

> 类文件 CWebUser

> 用来管理一个用户登录状态下的信息,基于用户名，密码和cookie,session。登录后创建的用户信息通过session保存。



* **配合一个CUserIdentity的子类实例，即可完成用户等登录、退出、管理状态数据。**

		典型流程:  检测用户是否处于登录状态 > 取出用户名和密码等验证数据 > 验证数据是否匹配 > 保存登录状态
	
		CUserIdentity的构造方法接收两个参数：username,password。username以是字符，也可以是数字uid值等任何可以区分用户的唯一标识。它的子类需要实现authenticate()验证方法，用来执行匹配验证，并且在最后必须设置$this->errorCode值为一个CUserIdentity的类常量，然后 return !$this->errorCode;。
	
		user组件在登录时将把CUserIdentity的子类的实例中的属性保存到user组件的对应属性中，完成登录数据保存。

### CWebUser 
<table>
	<thead>
		<tr>
			<th>可配置属性</th>
			<th>说明</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>allowAutoLogin</td>
			<td>是否允许自动登录，默认为false</td>
		</tr>
		<tr>
			<td>guestName</td>
			<td>访客显示的默认用户名</td>
		</tr>
		<tr>
			<td>loginUrl</td>
			<td>登录页面的route,array('/site/login','param1'=>'value1',...)</td>
		</tr>
		
		<tr>
			<td>returnUrl</td>
			<td>登录成功后跳转的目标</td>
		</tr>
		
		<tr>
			<td>stateKeyPrefix</td>
			<td>配置用来保存用户state信息的session变量的前缀</td>
		</tr>
		
		<tr>
			<td>authTimeout</td>
			<td>认证有效期，默认为会话。关闭浏览器即退出。</td>
		</tr>
		<tr>
			<td>autoRenewCookie</td>
			<td>是否自动重续认证cookie的有效期，默认为false，以密码登录时为认证有效期起点。如果设为true，以最近一次自动登录的时间为起点。仅在自动登录开启后有效</td>
		</tr>
		<tr>
			<td>autoUpdateFlash</td>
			<td>默认为true,meaning flash messages will be valid only in the current and the next requests.</td>
		</tr>
		<tr>
			<td>loginRequiredAjaxResponse</td>
			<td>如果退出后，继续请求一个需要登录状态才允许访问的页面所返回的内容。</td>
		</tr>
		<tr>
			<td>identityCookie</td>
			<td>仅在自动登录开启后有效。一个数组，配置用于认证的cookie的相关属性，默认值：array('domain'=>''，'path'=>'/'，'secure'=>false，'httpOnly'=>false)</td>
		</tr>
		
		<tr>
			<td> <strong>信息属性</strong></td>
			<td> <strong>建议通过get和set方法存取</strong></td>
		</tr>
		<tr>
			<td>flashes</td>
			<td>Flash messages array (key => message).</td>
		</tr>
		<tr>
			<td>name</td>
			<td>当前用户的用户名，如果是访客，返回guestName属性的值</td>
		</tr>
		<tr>
			<td>id</td>
			<td>当前用户的用户唯一ID，如果是 Null，表示是一个访客。可以使用数据表的主键或unique字段</td>
		</tr>
		<tr>
			<td>isGuest</td>
			<td>当前用户是否是访客</td>
		</tr>
	</tbody>
</table>

<table>
	<tr>
		<th>方法</th>
		<th>说明</th>
	</tr>
	<tr>
		<td>login(IUserIdentity $identity,$duration=0)</td>
		<td>用户登录，它将把IUserIdentity实例中的属性保存到用户session变量中</td>
	</tr>
	<tr>
		<td>clearStates()</td>
		<td>删除所有用户session变量，非登录状态session变量不会被删除。session cooike不会失效</td>
	</tr>
	<tr>
		<td>logout($destroySession=true)</td>
		<td>彻底退出登录，默认会删除这个用户的所有session数据使session cookie失效</td>
	</tr>
	<tr>
		<td>getName()</td>
		<td></td>
	</tr>
	<tr>
		<td>setName($value)</td>
		<td></td>
	</tr>
	<tr>
		<td>getId()</td>
		<td></td>
	</tr>
	<tr>
		<td>setId($value)</td>
		<td>登录成功后，你必须调用它设定一个唯一值，否则仍会作为游客。</td>
	</tr>
	<tr>
		<td>getIsGuest()</td>
		<td></td>
	</tr>
	<tr>
		<td>getReturnUrl($defaultUrl=null)</td>
		<td>获得登录成功返回页面，默认返回首页</td>
	</tr>
	<tr>
		<td>setReturnUrl($value)</td>
		<td>手动指定登录成功后返回的页面</td>
	</tr>
	<tr>
		<td>loginRequired()</td>
		<td>跳转到登录页面，这种情况下登录成功后返回登录前的页面</td>
	</tr>
	<tr>
		<td>setState($key,$value,$defaultValue=null)</td>
		<td>设置一个用户session变量，当$value与$defaultValue相等，表示要删除此变量。即默认将$value设为null即删除此变量</td>
	</tr>
	<tr>
		<td>getState($key,$defaultValue=null)</td>
		<td>读取一个用户session变量</td>
	</tr>
	<tr>
		<td>hasState($key)</td>
		<td>判断是否有某个用户session变量</td>
	</tr>
	<tr>
		<td>setFlash($key,$value,$defaultValue=null)</td>
		<td></td>
	</tr>
	<tr>
		<td>hasFlash($key)</td>
		<td></td>
	</tr>
	<tr>
		<td>getFlash($key,$defaultValue=null,$delete=true)</td>
		<td></td>
	</tr>
	<tr>
		<td>getFlashes($delete=true)</td>
		<td></td>
	</tr>
</table>

	flash message 是一次性变量，即保存以后，在读取后自动从用户session中删除这条变量。


### 可扩展的空方法：

* beforeLogin($id,$states,$fromCookie)
* afterLogin($fromCookie)
* beforeLogout()
* afterLogout()

```php
<?php
...
// 使用提供的用户名和密码登录用户
$identity=new UserIdentity($username,$password);
if($identity->authenticate()){
    Yii::app()->user->login($identity);
}else{
    echo $identity->errorMessage;
}......
// 注销当前用户
Yii::app()->user->logout();
```

Yii 控制器内预定义了一个filterAccessControl过滤器方法，可以进行基于ip和用户名的访问控制。

要使用这个过滤器，先在filter()方法中注册这个过滤器，然后在控制器中创建一个accessRules()方法返回一个控制规则数组：
```php
class PostController extends CController{
    ......
    public function filters(){
        return array(
            'accessControl',
        );
    }
    public function accessRules(){
        return array(
                array(
                    'deny',
                    'actions'=>array('create', 'edit'),
                    'users'=>array('?'),
                ),
                array(
                    'allow',
                    'actions'=>array('delete'),
                    'roles'=>array('admin'),
                ),
                array(
                    'deny',
                    'actions'=>array('delete'),
                    'users'=>array('*'),
                ),
        );
    }
                  
}
```


1. 必选项：第一个元素值是deny或者allow,没有key
2. 可选项：第二个元素的key为'controllers',指定规则对哪些controller生效;
2. 必选项：第三个元素的key为'actions',指定规则对哪些action生效;
4. 可选项：key为'message',value为验证失败后显示的消息。
5. 可选项：key为'deniedCallback'，value为验证失败后执行的回调函数，回调参数是引起失败的这条规则。
6. 可选项：key为'expression'，value是一个php表达式或一个callable。它的值用来表明这条规则是否适用。
		
		在表达式,你可以直接使用一个叫$user的变量,在执行过程它会被赋值为 Yii::app()->user ,表达式计算结果必须是一个布尔值。
		如果value是callable，它将被传入一个数组作为参数array('user'=>$user,$this)，返回值为布尔值。
7. 余下的元素的key分别是'users','roles','ips',''verbs','expression'，值都是数组。
	* users: 设置哪些用户匹配此规则。 当前用户的 name 被用来匹配。这里可以使用三种特殊字符:
 		* * : 任何用户,包括匿名和验证通过的用户。
 		* ? : 匿名用户。
 		* @ : 验证通过的用户。
	* roles: 设定哪些授权角色、授权任务、授权操作匹配此规则。 这里用到了将在下一节描述的特征。特别的, 若CWebUser::checkAccess 为其中一个角色返回 true ,此规则被应用。
	
			提示,本项在第一个元素为 allow 时使用,因为角色授权隐含着allow的含义。
			
	* ips: 设定哪些客户端 IP 匹配此规则。
	* verbs: 设定哪些请求类型(例如: GET, POST )匹配此规则。不区分大小写。
	
当一个用户的 **user,ip,verb,controller,action** 都符合规则时，用户才获得访问权限。

## authManager

> 提供RBAC功能

> 方案：CPhpAuthManager(默认)，CDbAuthManager

### 概念

* authorization item，授权项目
	
		CAuthItem 的实例，需使用authManager创建，请不要手动new。有三种授权类型：
		
	1. roles > 它的下级授权项目是tasks 和其他 roles
	2. tasks > 它的下级授权项目是operations 和其他 tasks
	3. operations 它不需要下级授权项目，也可以把其他operations作为下级授权项目
* 权限继承：每个授权项目都可以定义它的业务规则，上级授权项目拥有下级授权项目具有的权限
* 认证项目名称必须惟一
* 业务规则bizrules,用来对授权项目进行额外的限制，是一段执行结果为布尔值的表达式或者返回值为布尔值的callable，当它返回值为真时，授权才成立。默认为空，表示
* 附加数据 授权项目和授权分配实例在创建时都可以传入附加收据参数。
* 授权分配信息 
		CAuthAssignment实例,需使用authManager分配，请不要手动new。
* 授权数据  即CAuthItem和CAuthAssignment实例。CPhpAuthManager的授权数据转为数组存储在一个文件中，CDbAuthManager的授权数据保存为一个数据表。


### CAuthManager

<table width="100%">
	<thead>
		<tr>
			<th>基本配置属性</th>
			<th>说明</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>showErrors</td>
			<td>Enable error reporting for bizRules.</td>
		</tr>
		<tr>
			<td>defaultRoles</td>
			<td>分配给所有用户的授权项目列表，一个授权项目一个元素。(不仅限于角色，也可以是任务和操作)</td>
		</tr>
		<tr>
			<td> <strong>CDbAuthManager配置属性</strong></td>
			<td></td>
		</tr>
		<tr>
			<td>connectionID</td>
			<td>默认值id</td>
		</tr>
		<tr>
			<td>itemTable</td>
			<td>存储authorization items的数据表,默认值：AuthItem</td>
		</tr>
		<tr>
			<td>itemChildTable</td>
			<td>存储授权等级的表，默认值：'AuthItemChild'</td>
		</tr>
		<tr>
			<td>assignmentTable</td>
			<td>存储业务规则分配的表， 默认值：'AuthAssignment'</td>
		</tr>
		<tr>
			<td> <strong>CPhpAuthManager配置属性</strong></td>
			<td></td>
		</tr>
		<tr>
			<td>authFile</td>
			<td>RBAC认证文件存储位置，默认为：'protected/data/auth.php'</td>
		</tr>
	</tbody>
</table>


<table width="100%">
	<thead>
		<tr>
			<th>方法</th>
			<th>说明</th>
		</tr>
	</thead>
	<tbody>
			<td>createAuthItem($name,$type,$description='',$bizRule=null,$data=null)</td>
			<td>创建授权项目。$type  (0: operation, 1: task, 2: role).</td>
		</tr>
		<tr>
			<td>getAuthItems($type=null,$userId=null)</td>
			<td>获取某种类型的所有授权项目。如果提供了userId,则获取分配到这个用户上的所有授权项目。$type  (0: operation, 1: task, 2: role).</td>
		</tr>
		<tr>
			<td>getAuthItem($name)</td>
			<td>取出某个授权项目的CAuthItem实例</td>
		</tr>
		<tr>
			<td>saveAuthItem($item,$oldName=null)</td>
			<td>保存修改后的授权项目，如果授权项目名称没有改变，无需提供$oldName，否则必须提供$oldName</td>
		</tr>
		<tr>
			<td>removeAuthItem($name)</td>
			<td>删除一个授权项目</td>
		</tr>
		<tr>
			<td>addItemChild($itemName,$childName)</td>
			<td>为$itemName授权添加一个直接下级授权项目，名称为$childName</td>
		</tr>
		<tr>
			<td>removeItemChild($itemName,$childName)</td>
			<td>取消$itemName和$childName的直接上下级关系</td>
		</tr>
		<tr>
			<td>hasItemChild($itemName,$childName)</td>
			<td>授权项目是否有一个直接下级授权项目$childName</td>
		</tr>
		<tr>
			<td>getItemChildren($itemName)</td>
			<td>返回一个或几个授权项目(数组)的所有直接下级授权项目</td>
		</tr>
		<tr>
			<td>assign($itemName,$userId,$bizRule=null,$data=null)</td>
			<td>分配一个授权项目给某个用户</td>
		</tr>
		<tr>
			<td>revoke($itemName,$userId)</td>
			<td>废除某个用户的授权项目</td>
		</tr>
		<tr>
			<td>isAssigned($itemName,$userId)</td>
			<td>判断某个用户是否拥有了某项授权</td>
		</tr>
		<tr>
			<td>getAuthAssignment($itemName,$userId)</td>
			<td>获取某个用户的某项授权分配实例</td>
		</tr>
		<tr>
			<td>getAuthAssignments($userId)</td>
			<td>获取某个用户的所有授权分配实例</td>
		</tr>
		<tr>
			<td>saveAuthAssignment($assignment)</td>
			<td>仅供CDbAuthManager保存修改后的授权分配到数据库</td>
		</tr>
		<tr>
			<td>clearAll()</td>
			<td>清除所有授权数据：包括授权分配，授权项目。CPhpAuthManager模式在最后需要save()</td>
		</tr>
		<tr>
			<td>clearAuthAssignments()</td>
			<td>只删除所有授权分配。CPhpAuthManager模式在最后需要save()</td>
		</tr>
		<tr>
			<td>save()</td>
			<td>仅供CPhpAuthManager用它把授权数据转为数组格式后保存到php文件中，所以当授权数据发生变化后，必须调用它来执行保存。</td>
		</tr>

		<tr>
			<td>load()</td>
			<td>仅供CPhpAuthManager用它来载入从php文件中载入授权数据，还原成CAuthItem和CAuthAssignment实例</td>
	</tbody>
</table>


```php
<?php
...
//至少需要创建两个默认角色：guest和authenticated，分别表示游客和通过验证用户
$bizRule='return !Yii::app()->user->isGuest;';
$auth->createRole('authenticated','authenticated user',$bizRule);
$bizRule='return Yii::app()->user->isGuest;';
$auth->createRole('guest','guest user',$bizRule);
```


```php
<?php
...
if(Yii::app()->user->checkAccess('createPost'))
{
    // 创建发布
}
```


## Yii::app()->user->checkAccess('authItemName')

> 执行对某个 operation授权项目 的权限检查，已经检查通过后不会重复检查。条件如下：

* 检测通过的必要条件：
	1. 检测的授权项目T存在
	2. 授权项目T 和 授权分配实例a1 上的 bizRule 执行为结果为 真。
* 检测通过的充分条件：
	* 授权项目T 是 默认授权项目
	* 授权项目T 被分配给了这个用户，且这项分配a2 附加的 bizRule执行结果为真。
	* 授权项目A是 的 授权项目T 的上级授权项目，且授权项目A经过同样的检测规则后通过检测。
