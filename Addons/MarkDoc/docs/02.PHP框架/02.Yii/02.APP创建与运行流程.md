 
# YiiBase

> 提供了一些核心方法，主要在创建app过程中调用，包括类库自动载入，创建app,创建组件。<br>
> 还提供了一些核心工具：import(),beginProfile(),endProfile(),log(),trace(),setPathOfAlias(),getPathOfAlias(),t()
> createComponent($config)

**Yii::createComponent($config)** 是一个核心工具，用于根据属性初始配置$config,创建一个CComponent类的子类的实例。也就是说它可以用来创建Yii中的几乎所有类的实例。Yii执行过程中的各种实例创建，大部分是由这个方法完成。

### app可配置项

 	'name'=>'网站项目',
 	'charset'=>'utf-8',
 	'sourceLanguage'=>'zh_cn',
	'defaultController'=>'site',
 	'layout'=>'main',
 	'TimeZone'=>'aisa/shanghai',
 	'homeUrl'=>'',
 	'baseUrl'=>'',
 	'catchAllRequest'=>array(),
 	'controllerMap'=array(),

 	'theme'=>'',
 	'setViewPath'=>'',
 	'systemViewPath'=>'',
 	'layoutPath'=>'',
 	
 	'behaviors'=array(),
 	'aliases'=>array(),
 	'preload'=>array(),
 	'import'=>array(),
 	'modules'=>array(),
 	'components'=>array(),
 	'params'=>array(),

# APP实例

## 创建流程 createWebApplication()

	主要完成app实例属性的初始化和预定义组件载入,类库的载入

```php
Yii::createWebApplication($config)
```

1. 将app实例保存到 私有属性 $_app中
2. 设置basePath路径，定义 application，webroot,ext三个路径别名
3. 执行$this->preinit();
4. 注册 错误和异常处理handler,载入系统核心组件
5. 根据配置项，完成app实例的属性初始化组件和模块等配置, **类库的载入**
6. attach配置中的行为到app中，载入配置中preload项中的组件
7. 执行$this->init();

## 运行流程 run()
	主要完成请求解析，运行请求的控制器

```php
Yii::createWebApplication($config)->run()
```

1. 执行事件：`$this->onBeginRequest(new CEvent($this))`
2. 执行请求：`$this->processRequest();`
	* $route=$this->getUrlManager()->parseUrl($this->getRequest());
	* $this->runController($route);
3. 执行事件：`$this->onEndRequest(new CEvent($this))`

### 使用

	主要用于访问app实例中的属性和组件
```php
Yii::app();
```
用于返回当前的app实例

## app配置原理(以类库载入为例)

> 所有可配置项目在CModule(行为，别名，类库，模块，组件),CApplication(路径和url),CWebApplication(视图相关)的属性注释中有描述

1. CApplication::__construct($config)
2. CApplication::configure($config)
3. CApplication::$import=array(...)

    import属性在CModule,CApplication,CWebApplication中都不存在，触发CComponent::__set()方法，
4. CComponent::__set('import',array(...))
5. CModule::setImport(array(...)) 
```php
	public function setImport($aliases)
	{
		foreach($aliases as $alias)
			Yii::import($alias);
	}
```
6. Yii::import($alias)

## 扩展CWebApplication

>扩展CWebApplication类主要是增加一些配置属性，增加一些必要的事件，重写四个方法：

1. protected function **preinit**()
2. public function **beforeControllerAction**(CController $controller,CAction $action)
3. public function **afterControllerAction**($controller,$action)
4. protected function **init**()

    注意：应该在开头或最后调用父类的方法

**例如：**

```php
	protected function init()
	{
		parent::init();
		$this->getRequest();
	}
```

# CApplication

> 它是应用的基类，为所有类型的应用提供了通用的基本功能：环境配置(语言、字符集、时区、核心目录)，核心组件注册，错误和异常处理handler注册，语言区域相关功能，url创建，request事件和错误异常事件，全局状态管理

<table width="100%">
	<thead>
		<tr>
			<th width="200px">方法</th>
			<th>说明</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>end($status=0,$exit=true)</td>
			<td>终止app（执行onEndRequest事件）</td>
		</tr>
		<tr>
			<td>onBeginRequest($event)</td>
			<td>beginRequest事件</td>
		</tr>
		<tr>
			<td>onEndRequest($event)</td>
			<td>endRequest事件</td>
		</tr>
		<tr>
			<td>onError($event)</td>
			<td>onError事件</td>
		</tr>
		<tr>
			<td>onException($event)</td>
			<td>onException事件</td>
		</tr>
		<tr>
			<td>saveGlobalState()</td>
			<td>在请求结束后，自动保存全局状态变量到本地文件</td>
		</tr>
		<tr>
			<td>loadGlobalState()</td>
			<td>在请求开始时，自动从本地文件载入全局状态数据</td>
		</tr>
		<tr>
			<td>clearGlobalState($key)</td>
			<td>删除一条全局状态变量</td>
		</tr>
		<tr>
			<td>setGlobalState($key,$value,$defaultValue=null)</td>
			<td>设置一条全局状态变量</td>
		</tr>
		<tr>
			<td>getGlobalState($key,$defaultValue=null)</td>
			<td>读取一条全局状态变量</td>
		</tr>
	</tbody>
</table>