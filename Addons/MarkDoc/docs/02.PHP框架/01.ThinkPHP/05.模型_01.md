模型配置
======

mysql方式配置
-----------

	'DB_TYPE'=> 'mysql',         //驱动类型：mysql,mysqli,pdo
	'DB_HOST'=> 'localhost',
	'DB_NAME'=> 'thinkphp',
	'DB_USER'=> 'root',
	'DB_PWD' => '',
	'DB_PORT' => '3306',
	'DB_PREFIX' =>'think_',      //数据表默认前缀
	'DB_FIELDS_CACHE' =>false,   //关闭模型字段缓存，开发阶段关闭，部署阶段应开启
	'DB_LIKE_FIELDS'=>'title|content',//自动进行模糊查询的字段，多个用竖线分隔

pdo方式配置
-----------

	'DB_TYPE'=> 'pdo',
	'DB_USER'=> 'root',
	'DB_PWD' => '',
	'DB_PREFIX' =>'think_', //数据表默认前缀
	'DB_DSN'=>'mysql:host=localhost:3306;dbname=thinkphp;charset=UTF-8', //dsn方式优先级最高


数据表默认命名
--------

默认方式：UserTypeModel 对应  think_user_type

模型类属性
----------

> 修改protected属性可以通过 ** setProperty($name,$value) ** 方法

	private   $_extModel        =   null;
	protected $db               =   null;     //$db属性用于保存 “当前的数据库连接对象”，可以使用db(num)方法改变这个属性来创建或切换到其他连接对象
	protected $pk               =   'id';     //数据表的主键
	protected $tablePrefix      =   '';       // 数据表前缀
	protected $name             =   '';       // 自定义模型名称
	protected $dbName           =   '';       // 数据库名称
	protected $connection       =   '';       // 数据库连接配置
	protected $tableName        =   '';       // 数据表名（不包含表前缀）
	protected $trueTableName    =   '';       // 实际数据表名（包含表前缀）
	protected $error            =   '';       // 最近错误信息
	protected $fields           =   array();  // 表字段信息，创建模型对象后字段信息会保存在这里。也可硬编码在这里
	protected $_map             =   array();  // 表单字段映射定义：key为表单name,value为数据表字段名
	protected $data             =   array();  // 保存ceeate()创建的数据数组和find()方法取回的数据，二维数组，每行记录是一个一维元素
	protected $options          =   array();  // 查询表达式参数

	protected $_validate        =   array();  // 自动验证定义
	protected $_auto            =   array();  // 自动完成定义
	protected $_scope           =   array();  // 命名范围定义
	// 是否自动检测数据表字段信息
	protected $autoCheckFields  =   true;
	// 是否批处理验证
	protected $patchValidate    =   false;
	// 链操作方法列表
	protected $methods          =   array('table','order','alias','having','group','lock','distinct','auto','filter','validate');


模型实例化
======

空模型实例
-------

> 如果仅需要使用原生sql查询语句，实例化一个空模型类即可:

* $model = new Model();
* $model= D();
* $model =M();

`$model->query('select * from think_user where status=1');`

扩展模型类
---------

> ThinkPHP/Extend/Model提供了一些扩展模型类：可用 new 或 M函数 创建这些扩展模型类的对象

	ThinkPHP/Extend/Model
	├── AdvModel.class.php
	├── MongoModel.class.php
	├── RelationModel.class.php
	└── ViewModel.class.php


公共模型类
---------

> 项目中不是所有的表都有必要创建对应的模型类，可以创建一个公共的模型类封装一些常用的方法来处理这些表。通常，我们使用 CommonModel类 来处理这些表，那么就可以使用：

`new CommonModel($name='', $tablePrefix='',$connection='');` 或 `M('CommonModel:User','think_','db_config');`

自定义模型类：
----------

> 一个自定义模型类对应一个数据表，封装了对这个数据表进行快速操作的相关方法

#####创建模型实例的三种方式：

* 直接 `new Modelname($name='',$tablePrefix='',$connection='')`

	$name：数据表名称不含前缀

* 使用 `M($table_name='', $tablePrefix='',$dbconnection='')` 函数创建 内置模型类 实例

	$name参数格式： 'model_name:dbname.table_name'（表名称不含前缀）  例如 M('MongoModel:user')，M('Model::user')可以简写为M('user')

* 使用 `D($model_name='',$layer='')`函数创建自定义模型类实例

	$name参数格式：'APP://Modelname'或'group/Modelname'

		$User = D('user');         //实例化当前项目或分组的user模型
		$User = D('admin/user');   //实例化admin分组的user模型
		$User = D('admin://user'); //实例化admin项目的user模型

多层模型支持
-------------

> 3.1版本开始，模型层（M）支持自定义分层。并且D方法，增加layer参数，具体分层的M类仍然继承Model类

例如: 文件位于项目的 Lib/Logic/UserLogic.class.php

实例化 UserLogic 类 实现 Logic 分层 `D('User','Logic');`

数据操作
=======

连接数据库
----------

> 除了可根据配置文件设置自动连接数据库，也可以在模型类属性中通过$connection属性设置独立的连接方式：


	protected $connection =array(
			'DB_TYPE'=> 'mysql',
			'DB_HOST'=> 'localhost',
			'DB_NAME'=> 'thinkphp',
			'DB_USER'=> 'root',
			'DB_PWD' => '',
			'DB_PORT' => '3306',
			'DB_PREFIX' =>'think_',//数据表默认前缀
		);

		
	protected $connection = 'mysql:host=localhost:3306;dbname=thinkphp;charset=UTF-8';

	
	//或者把连接配置数组或dsn字符串配置成一个自定义配置项，在属性中直接使用
	protected $connection = 'CUSTOM_DB_CONFIG';


切换/销毁数据库连接
------------

	$this->db(1,"mysql://root:123456@localhost:3306/test");//第一个参数为数据库编号，第二个参数为连接
	$this->db(1)->query("查诟 SQL");//数据库连接过一次以后，以后都可以直接使用

db()方法返回的是$this，所以支持连贯操作。第二个参数如果指定为NULL，则可以销毁一个数据库连接。


字段定义
---------

> TP模型类在实例化时候会自动获取并将字段信息保存到 **$fields**属性，生产环境下默认还将缓存字段信息到Runtime/Data/_fields/下的本地文件。

> 也可以把字段信息定义在$fields属性中，提高性能

	protected $fields=array('id','username','email','age','_pk'=>'id','_autoinc'=>true );

获取字段信息
-------


`Model::getDbFields()`， 如果设定了Model::option['table'],则根据其获取表的字段,否则(删除 _pk,_autoinc,_type元素后)返回Model::$field数组;如果Model::$field也不存在,返回false


CURD方法
==========

构造查询
------

伪方法：

table($table) ,alias($alias) ，having($having) ，distinct($bool) ，lock($bool)  ，group($group) ，order($order)

|连贯操作                            |说明                                                             |实例                                                                                                                                                                            |
|-------------------------------|-----------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|where($where,$parse=null)           |用于查询或者更新条件的定义                                       |-                                                                                                                                                                               |
|table($table)                       |用于定义要操作的数据表名称                                       |Table('think_user user') <br>Table('db_name.think_user user')<br>Table(array('think_user'=>'user','think_group'=>'group'))                                                      |
|alias($alias)                       |用于给当前数据表定义别名                                         |-|
|field($field,$except=false)         |如果不调用此方法相当于field('*')<br> {y:$except为true，则表示要排除的字段}|field('id,nickname as name')<br>field(array('id','nickname'=>'name'))                                                                                                           |
|data($data='')                      |用于为新增或者更新设置数据，参数可以是数组、queryString、数据型object，如果参数为空返回模型的$this->data数据                                             |-                                                                                                                                                          |
|order($order)                       |用于对结果排序                                                   |order('id desc');<br>order('status desc,id asc');<br>order(array('status'=>'desc','id'));                                                                                       |
|limit('offset,length')              |用于限制查询结果的数量                                           |limit('1,10')<br>如果使用 limit('10') 等效 limit('0,10')                                                                                                                        |
|page('page[,listRows]')             |用于查询分页(内部会转换成limit)                                  |例如:Page('2,10')<br>表示每页显示 10 条记录的情况的下面,获取第 2 页的数据。                                                                                                     |
|group($group)                       |用于对数据库的group查询                                          |group('user_id')                                                                                                                                                                |
|having($having)                     |用于对数据库的having支持                                         |having('user_id>0')                                                                                                                                                             |
|join($join)                         |-                                                                |join(' work ON artist.id = work.artist_id')                                                                                                                                     |
|union($union,$all=false)            |用于对数据库的union支持                                          |union('SELECT name FROM think_user_2')<br>union(array('field'=>'name','table'=>'think_user_2'))<br>union(array(' SELECT name FROMthink_user_1','SELECT name FROM think_user_2'))|
|distinct($bool)                     |查询数据的旪候进行唯一过滤                                       |$Model->Distinct(true)->field('name')->select();                                                                                                                                |
|{g:cache($key=true,$expire='',$type='')}|查询结果是否缓存                                                 |-                                                                                                                                                                               |
|lock($bool)                         |用于数据库的锁机制                                               |如果在查询或者执行操作的时候使用:Lock(true)就会自动在生成的 SQL 语句最后加上 FOR UPDATE 或者 FOR UPDATE NOWAIT  (Oracle数据库)                                                  |
|scope($scope)                      |使用查询范围|`$Model->scope('new,new100')->select();`|

查询范围
---------

范围定义和使用
--------

```
class NewsModel extends Model {
    protected $_scope = array(
    		'new'=>array(	'where'=>array('status'=>1),'order'=>'create_time DESC','cache'=>array('key'=>'NEWS_CACHE'),),
            'new100'=>array('limit'=>100,),
    );
}
```

`$Model->scope('new,new100')->select();`


### 支持的属性


```
+----------+------------+
| where    | 查询条件   |
+----------+------------+
| field    | 查询字段   |
+----------+------------+
| order    | 结果排序   |
+----------+------------+
| table    | 查询表名   |
+----------+------------+
| limit    | 结果限制   |
+----------+------------+
| page     | 结果分页   |
+----------+------------+
| having   | having查询 |
+----------+------------+
| group    | group查询  |
+----------+------------+
| lock     | 查询锁定   |
+----------+------------+
| distinct | 唯一查询   |
+----------+------------+
| cache    | 查询缓存   |
+----------+------------+
```

执行查询
---------

> sql执行方法跟在一连串sql构造方法的最后，用来根据构造的sql条件进行CRUD操作

### Insert

<p class="api">Model::add($data='',$options=array(),$replace=false)</p>

* $data  数据数组。为空表示使用$this->data
* $replace为true则插入冲突时覆盖原有数据

* 如果数据非法或者查询错误则返回false
* 如果是自增主键 则返回主键值，否则返回1

<p class="api">Model::addAll($data='',$options=array(),$replace=false)</p>

{r:该方法仅支持mysql数据库的驱动}

* $data  二维数组。
* $replace为true则插入冲突时覆盖原有数据

* 如果数据非法或者查询错误则返回false
* 如果是自增主键 则返回最后一条插入的记录的主键

### Update

<p class="api">Model::save($data='',$options=array())</p>

* data：要保存的数据，如果为空，则取当前的数据对象。
* options：为数组的时候表示操作表达式，通常由连贯操作完成；为数字或者字符串的时候表示主键值。默认为空数组。
* 如果查询错误或者数据非法返回false
* 如果更新成功返回影响的记录数

<p class="api">Model::setField($field,$value='')</p>

* field:要更新的字段名，更新多个字段使用key／value数组。
* value:要更新的值，当 field 为数组时 value 值无效。

<p class="api">Model::setInc($field,$step=1)</p>

整型字段值增长
<p class="api">Model::setDec($field,$step=1)</p>

整型字段值减少

	$User->where('id=5')->setInc('score');   // 用户积分加 1
	$User->where('id=5')->setDec('score',5); // 用户积分减 5

### Delete

<p class="api">Model::delete($options=array())</p>

* 为数字或者字符串的时候表示主键值 
* 如果查询错误返回false
* 如果删除成功返回影响的记录数

	$User->delete('5,6'); // 删除主键为5、6的多个数据

	

### Select

<p class="api">Model::getField($field,$sepa=null)</p>

* field（必须）：要获取的字段字符串（多个用逗号分隔）
* 如果查询结果为空返回null
* 如果field是一个字段，则只返回该字段查询结果的第一个值
* 如果field是多个字段，返回数组：每行是一个一维元素，第一个字段的值作为元素的key，如果sepa为null则每一行又是一个关联数组。索引是第一个字段的值，

##### getField('username,id,password',',')返回示例：

	array (size=3)
	'zhu' => string 'zhu,0,111111' (length=12)
	'liu' => string 'liu,1,111111' (length=12)
	'zhang' => string 'zhang,3,123456' (length=14)


##### getField('username,id,password')返回示例：

	array (size=3)
	'zhu' =>
		array (size=3)
		'username' => string 'zhu' (length=3)
		'id' => string '0' (length=1)
		'password' => string '111111' (length=6)
	'liu' =>
		array (size=3)
		'username' => string 'liu' (length=3)
		'id' => string '1' (length=1)
		'password' => string '111111' (length=6)
	'zhang' =>
		array (size=3)
		'username' => string 'zhang' (length=5)
		'id' => string '3' (length=1)
		'password' => string '123456' (length=6)



<p class="api">Model::find()</p>
读取一行记录

* options（可选）：为数组的时候表示操作表达式，通常由连贯操作完成；为数字或者字符串的时候表示主键值。默认为空数组。
* 如果查询错误返回false
* 如果查询结果为空返回null
* 只能返回一行记录，查询成功返回查询的结果（索引数组）

#####find(1)查询结果示例：

		array (size=4)
		'id' => string '1' (length=1)
		'password' => string '123456' (length=6)
		'username' => string 'liu' (length=3)
		'postdate' => null
  

<p class="api">Model::select($options=array())</p>

options（可选）：为数组的时候表示操作表达式，通常由连贯操作完成；如果是数字或者字符串表示主键值。默认为空数组。
查询错误返回false
查询结果为空返回null

##### 查询成功返回查询的结果集（二维索引数组）

$model->select('1,3');

	array (size=2)
	0 =>
		array (size=4)
		'id' => string '1' (length=1)
		'password' => string '111111' (length=6)
		'username' => string 'liu' (length=3)
		'postdate' => null
	1 =>
		array (size=4)
		'id' => string '3' (length=1)
		'password' => string '123456' (length=6)
		'username' => string 'zhang' (length=5)
		'postdate' => string '2012' (length=4)


###sql事件方法：

> 在自定义模型类中实现的方法，否则不起任何作用

> $data是要操作的数据，$options是查询条件

##### _before*和_after_find,_after_select 中接收的是引用，用于处理结果和执行前对数据的预处理

	_after_db()                         // 切换数据库后自动调用
	_after_delete ($data,$options)      // 执行完delete后自动调用
	_after_insert ($data,$options)      // 执行完add()后自动调用
	_after_update ($data,$options)      // 执行完save()后自动调用
	_before_insert(&$data,$options)
	_before_update(&$data,$options)
	_before_write (&$data)              // 写入数据前的回调方法 包括新增和更新
	_after_find   (&$result,$options)   // 执行完find()方法后自动调用
	_after_select (&$resultSet,$options)// 执行完Select()方法后自动调用

	

##### 常用信息获取方法：

<p class="api">getDbError()</p>
<p class="api">getError()</p>
<p class="api">getLastInsID()</p>
<p class="api">getLastSql()</p>
<p class="api">getTableName()</p>
<p class="api">getModelName()</p>
<p class="api">getDbFields()</p>
<p class="api">getPk()</p>

#####事务方法：

<p class="api">startTrans()</p>
<p class="api">commit()</p>
<p class="api">rollback()</p>


