控制器
=========

控制器实例化过程
-------

`App::run()--->App::exec()-->$module  =  A($group.MODULE_NAME);`

操作执行过程
-------------

`App::exec()--->$method =   new ReflectionMethod($module, $action);--->$method->invoke($module);`

控制器的属性：
-------------

    protected $view     =  null; //视图实例对象
    private   $name     =  ''; //当前控制器名称
    protected $tVar     =   array(); //模板变量
    protected $config   =   array(); //控制器参数  将传入行为类的run($param)方法


模板与控制器
-------------

模板与控制器action的对应关系：

* **默认：** `?m=module&a=action`  对应  `APP/Tpl/module/action.html`
* **单文件方式**：配置`'TMPL_FILE_DEPR'=>'_'`，则  `?m=module&a=action`  对应 `APP/Tpl/module_action.html`
* **输出纯html文件：** 如果访问的action方法不存在，但模板文件action.html存在，则直接输出action.html


基础控制器
------------

* **默认控制器：**访问入口文件没有使用任何url参数，则调用IndexAction的index方法

* **魔术控制器：**`EmptyAction`  即访问不存在的模块时调用该模块

* **魔术操作：**`_empty()` 即访问不存在的操作时调用该操作


URL配置
===============

url配置项
------------

	'URL_MODEL' => 1,                    //url模式(0:普通模式;1:pathinfo模式;2:url_rewrite模式;3.兼容模式)
	'URL_PATHINFO_DEPR' => '/',          //pathinfo模式url分隔符,默认为/斜线
	'URL_CASE_INSENSITIVE' =>false    ,   //url不区分大小写，建议保持默认
	'URL_HTML_SUFFIX' => '.shtml',       //伪静态后缀:设置了该项以后url末尾的.shtml才会被当作文件后缀处理;否则会被当作GET参数传入脚本
	'URL_ROUTER_ON'=>TRUE,               //开启路由功能
	'URL_ROUTE_RULES'=>array(),          //路由规则定义


PATHINFO 模式(默认模式):
--------------------

> module即控制器名(不含Action,默认区分大小写，因为要载入的是对应的类文件)，action即控制器类的公共方法名(不区分大小写)

> http://localhost/app[/index.php]/module/action

> 如果主入口文件为index.php，且index.php位于项目目录下面，可以省略index.php。

例如：

* http://serverName/app/index.php/module/action?id=1 或者
* http://serverName/app/index.php/module/action/id/1/

普通模式：
------------

http://serverName/app/index.php?m=module&a=action&id=1

url_rewrite模式
---------------

> 使用rewrite规则重写url

##### 去index.php 的 rewrite规则：

	<IfModule mod_rewrite.c>
	RewriteEngine on
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteRule ^(.*)$ index.php/$1 [QSA,PT,L]
	</IfModule>


兼容模式
----------

http://serverName/appName/index.php?s=/module/action/id/1/


模块分组
===========

> 即存在多个APP，每个APP作为项目的一个模块而存在

config.php配置：
----------------

	'APP_GROUP_LIST'=>'HOME,APP1,APP2',  //多个用逗号隔开
	'DEFAULT_GROUP'=>'HOME',             //默认分组，只能设一个
	'APP_GROUP_MODE' => 1,				  //设为1时独立分组，为0是普通分组
	'TMPL_FILE_DEPR'=>'/',               //分组模板下面模块和操作的分隔符
	'VAR_GROUP'=>'g',                    //普通URL模式的分组的GET变量名
	'APP_GROUP_PATH' => `Modules` 		  //独立分组的目录，默认值为Modules

普通分组目录结构：
-----------

	Projetc/
	├── Common
	│   ├── Common.php 公共函数
	│   ├── Home
	│   │   └── functions.php Home组函数(用load()载入)
	│   └── Admin
	│       └── functions.php Admin组函数(用load()载入)
	├── Conf
	│   ├── config.php 公共配置
	│   ├── Home
	│   │   └── config.php    Home组配置
	│   └── Admin
	│       └── config.php    Admin组配置
	├── Lang
	│   ├── zh-cn
	│   │   ├── common.php 公共语言包
	│   │   └── module_name.php 模块语言包
	│   ├── Home
	│   │   └── zh-cn
	│   │       └── module_name.php  Home组的模块语言包
	│   └── Admin
	│       └── zh-cn
	│           └── module_name.php  Admin组的模块语言包
	├── Lib
	│   ├── Action
	│   │   ├── IndexAction.class.php
	│   │   ├── Home
	│   │   │   └── IndexAction.class.php
	│   │   └── Admin
	│   │       └── IndexAction.class.php
	│   ├── Behavior
	│   ├── Model
	│   └── Widget
	├── Runtime
	│   ├── Home
	│   └── Admin
	├── Tpl
	│   ├── Home
	│   └── Admin
	└── index.php

	
独立分组目录结构(3.1.2)
-----------

> 独立分组除了没有入口文件外，其他独立项目具备的结构都基本具备了

##### Home分组目录

	APP
	└─Modules
			└─Home
				├─Common 分组函数目录
				├─Conf 分组配置目录
				├─Lang 分组语言包目录
				├─Action 分组Action控制器目录
				├─Model 分组Model模型目录
				├─Widget 分组Widget目录
				├─ORG 分组扩展类库目录
				├─... 其他分层目录
				└─Tpl 分组模板目录


分组模式URL
-------------

(以Admin分组下的Index模块的index操作为例)

* pathinfo模式1 : http://serverName/index.php/Admin/Index/index
* 普通模式 : http://serverName/index.php?g=Admin&m=Index&a=index
* 兼容模式 : http://serverName/index.php?s=/Admin/Index/index

分组模式模板目录
----------

**默认方式：** `?g=group&m=module&a=action`  对应 `APP/Tpl/group/module/action.html`

单文件方式：'TMPL_FILE_DEPR'=>'_'配置下，`?g=group&m=module&a=action`  对应 `APP/Tpl/group/module_action.html`


路由规则：
============

> 开启路由功能并且配置 **URL_ROUTE_RULES** 参数后,系统会自动进行路由检测，如果当前url的query部分与"路由规则"定义匹配,就会进行路由解析和重定向

路由规则可用于网站改版后的URL迁移重定向，搜索引擎将记录网址迁移

	'URL_ROUTE_RULES'=>array(
		'规则定义'=>'解析目标',
	),
	
解析目标可以是
-----------

* '外部地址'
* 一个 pathinfo格式的 Query Sting：`分组/模块/操作?key1=value1&key2=value2`
* 一个保存有分组/模块/操作和GET变量的数组：`array('group/module/action','key1=value1&key2=value2...')`
* `array('外部地址','重定向代码')`

规则定义
----------

* 路由规则中如果以“:”开头，表示该匹配位是一个动态的可变匹配，而且匹配数据将保存到GET变量

* 动态匹配可使用约束：\d 表示只允许匹配数字，例如: 'news/:id\d'=>'News/read' ,表示id为数字的时候才匹配

* 动态匹配可使用排除：^表示排除，可使用 | 设置多个排除，例如 'news/:cate^add|edit|delete'=>'News/category'

* 路由规则支持完整匹配定义,例如:'news/:id\d$'=>'News/read'

* 路由规则中的固定匹配不区分大小写

* 可以使用正则表达式字符串作为规则

* 目标地址中可以直接使用规则定义中的可变匹配的value， 采用 :1、:2 的方式,表示第一个变量,第二个变量

#####举例,对于以下规则：

	'URL_ROUTE_RULES'=>array(        //路由规则定义
			'news/:year/:month/:day/'=>array('News/archive','status=1'),
			'news/:id'=>'News/read',
			'news/read/:id'=>'news:1',
			'/^blog\/(\d+)$/'=>'Blog/read?id=:1',
		),

* `http://serverName/index.php/news/8`                   解析为：`http://serverName/index.php/News/read`
* `http://serverName/index.php/news/2012/01/08`      解析为：`http://serverName/index.php/News/archive/status/1`
* `http://serverName/index.php/news/read/8`          解析为：`http://serverName/index.php/news/8`
* `http://serverName/index.php/blog/12`                 解析为：`http://serverName/index.php/Blog/read?id=12`

##完整匹配

* 'new/:cate$'=> 'News/category',可以匹配http://serverName/index.php/new/info，但不会匹配http://serverName/index.php/new/info/2 
* 而'new/:cate'=> 'News/category',则也可以匹配http://serverName/index.php/new/info/2

##URL生成：

在控制器中使用url时，使用 **U函数**和配置文件的相关配置返回URL

`U('group/module/action@domain.com?key=value&key2=value2' [,'参数','伪静态后缀','是否跳转','显示域名'])`

举例，配置为pathinfo模式：

`U('Blog/read@blog.thinkphp.cn','id=1'); ` 将返回：http://blog.thinkphp.cn/test/index.php/blog/read/id/1

调用其他模块
===========

> 由于当前APP或分组中的Action类都可以自动载入，因此调用其他模块直接 new 一个其他模块的对象即可。

如果要调用其他分组或APP中的模块，使用函数：`A ('[项目名://][分组名/]模块名')`

	A('User') 表示调用当前项目的User 模块
	A('Admin://User') 表示调用 Admin 项目的 User 模块
	A('Admin/User') 表示调用 Admin 分组的User 模块
	A('Admin://Tool/User') 表示调用 Admin 项目 Tool 分组的User 模块

如果要调用其他分组或APP中的模块操作，使用函数：`R('[项目名://][分组名/]模块名/操作名',array('参数 1','参数 2'...))`

页面跳转
=============

页面跳转是通过dispatch_jump.tpl模板中js完成的，有两个模板：

	'TEPL_ACTION_ERROR'   => THINK_PATH.'Tpl/dispatch_jump.tpl',
	'TEPL_ACTION_SUCCESS' => THINK_PATH.'Tpl/dispatch_jump.tpl',

实现跳转只需调用 Action基类内置的两个跳转方法 **success()** 和 **error()** ，而且可以支持 ajax 提交

	$this->assign('var2','value2');//跳转之前可以向出错模板和成功模板分配模板变量
	$this->success($message,$jumpUrl=''[,$ajax=false])
	$this->error  ($message,$jumpUrl=''[,$ajax=false])

!!!! {r:error和success方法会检测当前请求方式是否为ajax方式}，如果是ajax请求或者明确指定了参数$ajax=true，则以ajaxReturn方式返回json数据{status:'1',info:'',url:''},数据中的status为1对应success方法，为0对应error方法，info对应$message,url对应jumpurl.

