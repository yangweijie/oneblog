载入执行
========

## 嵌入html

```html
&lt;script  type="text/javascript"&gt;jscode&lt;/script&gt;
&lt;script  type="text/javascript  src="xxx.js">&lt;/script&gt;

&lt;a href="javascript:alert('test')"&gt; text&lt;/a>
&lt;form action="javascript:alert('test')"&gt;&lt;/form&gt;
```

变量
========

规则
-----

命名规则
: 只能以字母、$、_开头，不能是系统关键字，不能使用运算符

大小写
: 区分大小写

建议命名方式
: 函数和变量用驼峰法；对象首字母大写；

语句结束符
: ';'号和换行都视为一行语句结束

注释
:  //单行                  /*  多行注释*/

声明
: `var 变量名=值;`

声明多个变量
: `var 变量名1=值1,变量名2 =值2;`

声明值相等的多个变量
: `var  变量名1=变量名2=值;`{r:当值不为字符串或数值时，慎用此种方式，因为这可能表示多个变量指向同一个对象引用}

var 关键字
----------

var 关键字用于声明变量，由于javascript是基于对象的，所以声明的变量必然是以对象的属性保存。
全局变量，都以window对象的属性保存；而函数和方法中的局部变量,则在函数和方法运行时动态创建，
并以临时成员保存在window对象中(javascript通过内部机制保证临时变量不会覆盖全局变量)，运行结束后立即销毁，这也意味着，局部变量在函数和方法每次运行时都会执行初始化和赋值过程。

数据类型
==========

typeof
------

> 使用 `typeof` 运算符可以判断变量的数据类型,javascript有以下6种类型:

1. number 型(int float double NaN) 使用时要区分整数，浮点数（浮点数是近似数，大多情况浮点运算不能用 == 号进行比较判断）

	```js
	var a=10;
	var a=045;//八进制
	var a=0xff;//十六进制
	var f=10.5;//浮点数
	var f=10e+5;//科学计数法浮点数
	```

	```
	0.1+0.2==0.3  //false  运算精度导致
	0.1+0.1==0.2  //true  实际上执行的是0.1*2==0.2,故而返回true
	```
	
	NaN是一个特殊的数值，因为 `typeof NaN` 返回的是`number`，但NaN意思是“不是一个数值”,所以这为我们判断一个变量是否是真正人类
	理解的数字带来麻烦，可以使用jQurey.isNumeric()检测数字。也可以使用下面的自定义函数：
	
	```
	function isNumber(value){ return typeof value ==='number' && isFinite(value);}
	```

    {r:注意:javascript中,`NaN===NaN`返回false}

2. string 型

	单双引号没有区别,内部可以使用转义字符

	`+`号是字符串连接符

3. boolean 型

	等值为假的类型：`false   0   0.0  "0"  ""  null   undefined  []`

	等值为真的类型：`" ",{}`

4. object (object |Array | null| Regex |Date) 使用时分别处理

	数组和对象的元素值可以是任意类型的变量，也可以是含有变量的表达式
	
	由于 typeof 无法区分数组和对象，所以jQuery提供了区分的检测方法$.isArray()和$.isPlainObject()

    {r:特别注意的是,typeof认为null也是object!}
	

5. function

6. undefined

	#### 注意:

	**number，strings，undefined, null, booleans,** 这几种类型的变量值的原始值无法被改变，只能在副本中被改变然后将副本赋值给另一个变量，或者自身变量被重新赋值。

!!!!!由于javascript的类型区分及其笼统，所以最可靠的方式是使用jQuery等库提供的工具方法做类型检测

Object.prototype.toString.call()
--------------------------------

> 可以使用 `Object.prototype.toString.call()` 或  `Object.prototype.toString.apply()`得到更准确的类型信息

```
Object.prototype.toString.call([])
"[object Array]"
Object.prototype.toString.call({})
"[object Object]"
Object.prototype.toString.call('aa')
"[object String]"
Object.prototype.toString.call(1)
"[object Number]"
Object.prototype.toString.call(null)
"[object Null]"
Object.prototype.toString.call(function(){})
"[object Function]"
Object.prototype.toString.call(/a/)
"[object RegExp]"
Object.prototype.toString.call(undefined)
"[object Undefined]"
Object.prototype.toString.call(true)
"[object Boolean]"
Object.prototype.toString.call( new Date)
"[object Date]"
```
	
# 操作符 

算术运算符: `+   -   *   /    %    ++    --`

{r:注意：php取余运算符两边如果是浮点数会自动转为整型再取余；js中取余时不会自动转换为整数，所以要保证两边都是整数。}

取余结果的正负，由前面的数决定。

赋值运算符：  `=   +=   -=   *=    /=   %=`

条件运算符：  `<  >  ==  !=  >=  <=   ===   !==`

逻辑运算符：   `||   &&     !`

三元运算符：   `?  :`

位运算： `>>  << >>>  |  &  ~`

in：判断对象中是否存在某个属性或索引,in操作符的左边应该时一个对象的属性名字符串或者一个数组的索引号number;in操作符的右边必须时一个对象，否则会抛出异常

    对象语法：`v= str in Object`
    数组语法：`v= index in [...]`,判断数组中是否存在某个索引


# 类型转换

## 自动类型转换

> js根据语境需要，可以自动执行数据类型转换

> 注意：如果需要转换成number而类型无法转换为number，表达式会得到NaN

|原始值                    |String语境      |Number语境|Boolean语境|Object语境        |
|--------------------------|----------------|----------|-----------|------------------|
|undefined                 |"undefined"     |NaN       |false      |类型异常          |
|null                      |"null"          |0         |false      |类型异常          |
|true                      |"true"          |1         |-          |new Boolean(true) |
|false                     |"false"         |0         |-          |new Boolean(false)|
|""                        |-               |0         |false      |new String("")    |
|"1.2"                     |-               |1.2       |true       |new String("1.2") |
|"one"                     |-               |NaN       |true       |new String("one") |
|0                         |"0"             |-         |false      |new Number(0)     |
|-0                        |"0"             |-         |false      |new Number(-0)    |
|NaN                       |"NaN"           |-         |false      |new Number(NaN)   |
|Infinity                  |"Infinity"      |-         |true       |new Number(Infinity)|
|-Infinity                 |"-Infinity"     |-         |true       |new Number(-Inifinity)|
|1                         |"1"             |-         |true       |new Number(1)     |
|{}(任意对象)              |根据toString方法|-搜索一下 |true       |-                 |
|[]空数组                  |""              |0         |false      |-                 |
|[5,9]非空数组,元素为number|"5,9"           |5         |true       |-                 |
|['a']其他类型数组         |join方法        |NaN       |true       |-                 |
|function(){}函数          |搜一下          |NaN       |true       |-                 |


## 手动转换

使用new String,new Number,new Boolean可以按照以上规则转换数据

此外，字符串还可以通过.toString()方法获得

整型和浮点数可以通过以下方法获得：`parseInt(string);` `parseFloat(String);`

布尔型还可以通过!!符号转换自动转换为boolean：`!!a //相当于Boolean(a)`

数值型还可以通过变量前使用+号转换，但+号前面需要有一个空格或者用括号与其他变量分隔开(以免+号被解析为连字符)

```js
var b="2"; 
c=1+ +b;//=>3 建议加括号，防止js压缩后出错c=1+(+b);
```

## Number转为不同进制的字符串

```js
var n = 17; //n不能为字符串
binary_string = n.toString(2); // Evaluates to "10001"
octal_string = "0" + n.toString(8); // Evaluates to "021" 
hex_string = "0x" + n.toString(16); // Evaluates to "0x11"
```

比较运算中的类型转换
-------

进行 == 比较运算时，如果两边类型不同，javascript将尝试转换为相同类型进行比较，类型保留优先级：布尔型>number>string

!!!!! null和undefined与任何不为null/undefined的值进行==比较时，都返回false

其他
=====

保留字
-------

* abstract
* boolean
* byte
* char
* class
* const
* debugger
* double
* enum
* export
* extends
* fimal
* float
* goto
* implements
* import
* int
* interface
* long
* mative
* package
* private
* protected
* public
* short
* static
* super
* synchronized
* throws
* transient
* volatile
