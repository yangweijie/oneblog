mysqli基础类
=============

保存有数据库环境信息，执行查询后的执行结果信息

提供了连接、关闭数据库，执行sql查询的方法

```php
MySQLi {
/* 属性 */

//数据库环境信息属性
string $host_info;       //显示主机信息：Localhost via UNIX socket
string $protocol_version;//显示协议版本，例如：10
string $client_info;     //输出类似：mysqlnd 5.0.8-dev - 20102224 - $Id: 65fe78e70ce53d27a6cd578597722950e490b0d0 $
int    $client_version;  //输出类似：50008
string $server_info;     //输出类似：5.5.24-0ubuntu0.12.04.1，包括完整的数据库版本和运行平台
int    $server_version;  //输出mysql的数字版本号，类似：50524
int    $thread_id;       //输出连接的线程ID
//错误信息属性
string $connect_errno;   //返回连接数据库时的错误代码：1045
string $connect_error;   //返回连接数据库时的错误信息：Access denied for user 'root'@'localhost' (using password: YES)
int    $errno;           //返回上一条查询产生的错误代码：1193
string $sqlstate;        //
string $error;           //返回上一条查询产生的错误信息：Unknown system variable 'a'
array  $error_list;      //用数组返回上一次查询的错误信息：Array ([errno] => "1193", [sqlstate] =>" HY000", [error] =>" Unknown system variable 'a'" )
//查询语句返回信息：
int    $field_count;     //返回上一次select查询结果的字段数量
string $info;            //返回上一次查询的执行结果信息，类似：Records: 3 Duplicates: 0 Warnings: 0
mixed  $insert_id;       //返回insert/update执行后插入或修改的行的自增id的值
int    $affected_rows;   //返回 INSERT, UPDATE, REPLACE or DELETE 语句执行后的affect rows
int    $warning_count;   //返回查询产生的警告数量
/**** 方法 *********************************************************************/
__construct ([ string $host = ini_get("mysqli.default_host") [, string $username = ini_get("mysqli.default_user") [, string $passwd = ini_get("mysqli.default_pw") [, string $dbname = "" [, int $port = ini_get("mysqli.default_port") [, string $socket = ini_get("mysqli.default_socket") ]]]]]] )
bool    select_db ( string $dbname )   //选择要操作的库
string  stat ( void )                  //返回数据库的运行状况
bool    close ( void )                 //关闭连接
bool    kill ( int $processid )        //强制结束进程
string  character_set_name ( void )    //返回connection字符集
bool    set_charset (string $charset)  //设置字符集：utf8，gbk,gb2312；相当于set names命令
/*事务方法*/
bool    autocommit ( bool $mode )      //事务提交模式变更
bool    commit ( void )                //提交事务
bool    rollback ( void )              //回滚事务
/*查询方法*/
string  real_escape_string ( string $escapestr )
mixed   query (string $query [,int $resultmode = MYSQLI_STORE_RESULT]) //有结果显示的语句返回mysqli_result对象，其他语句返回布尔值，出错返回false
mysqli_stmt     prepare ( string $query )  //创建预处理查询语句对象
/* 尽量避免使用 multi_query 不利于错误处理 */
bool            multi_query ( string $query )     
bool            more_results ( void )   //检查是否还有multi_query的结果
bool            next_result ( void )
mysqli_result   store_result ( void )   //通常用这个接收multi_query返回的结果集创建mysqli_result对象
mysqli_result   use_result ( void )     //通常不用这个
}
```

mysqli_result对象处理类
===============

该类存有结果集的相关信息，负责从结果集进行数据提取

```php
mysqli_result implements Traversable {
/* 属性 */
int     $current_field ;  //获取当前字段的指针位置
int     $field_count;     //结果集的列数量
int     $num_rows;        //结果集的行数量
array   $lengths;         //fetch记录后，该属性存有该行记录各字段的字节长度(而非字符数量)，如果还没有开始fetch记录或者所有记录已fetch完，该属性为NULL

/* 方法 */
bool     data_seek ( int $offset )                     //移动指针到结果集的某一行
mixed    fetch_row ( void )                            //把指针所在行以索引数组返回，指针移动到下一行，直到返回null
array    fetch_assoc ( void )                          //把指针所在行以关联数组返回，指针移动到下一行，直到返回null
mixed    fetch_array ([int $resulttype = MYSQLI_BOTH]) //把指针所在行以索引数组返回，指针移动到下一行，直到返回null
object   fetch_object ([string $class_name [,array $params ]]) //把指针所在行以stdClass对象返回，指针移动到下一行，直到返回null;$params用于为对象附加成员属性
mixed    fetch_all ([int $resulttype = MYSQLI_NUM])    //把从当前指针位置开始的全部结果集以二维数组返回（参数指定关联数组，索引数组，或者兼有）

bool     field_seek ( int $fieldnr )                   //移动指针到结果集的某一列
object   fetch_field_direct ( int $fieldnr )           //返回某一列的meta-data对象
object   fetch_field ( void )                          //返回下一列的meta-data对象
array    fetch_fields ( void )                         //返回下一列的meta-data数组
void     free ( void )                                 //得到数据后释放结果集占用的内存是个非常重要的好习惯
}
```


mysqli_stmt预处理对象类
===============

预处理语句的占位符规定：只允许使用在诸如insert语句的values()或where子句的条件比较的值，不能用于表名和列名等标识符，不能与NULL比较，即“ ? IS NULL”这样是不允许的。一般情况，只允许用在数据操纵语句中，不能用于数据定义语句。

该类负责执行预处理查询，将执行结果信息保存在属性中，并可以直接以绑定字段值到变量的方式获取结果。

如果需要以数组方式按行整体返回数据，可以用getresult方法返回mysqliresult对象

```php
mysqli_stmt {
/**** 属性**********************************************/
/*查询失败信息*/
int    $errno;
string $error;
string $sqlstate;
array  $error_list;
/*查询成功信息*/
int    $affected_rows;
int    $insert_id;
/*数据信息*/
int    $field_count;
int    $num_rows;
/*占位参数数量*/
int    $param_count;
/**** 方法************************************************/
bool   bind_param ( string $types , mixed &$var1 [, mixed &$... ] ) //为预处理语句绑定参数
bool   execute ( void )                                             //执行查询，创建结果集
/*按字段直接获取结果数据*/
bool   bind_result ( mixed &$var1 [, mixed &$... ] )                //按列绑定excute的查询结果到变量
bool   fetch ( void )                                               //fetch一行查询结果，指针移动到下一行
void   data_seek ( int $offset )                                    //从结果集移动指针新的行位置
/*获取mysqli_result对象 按数组获取数据*/
bool   store_result ( void )                                        //创建结果集
mysqli_result get_result ( void )                                   //获得一个mysqli_result结果集对象
mysqli_result result_metadata ( void )                              //返回结果集metadata的mysqli_result对象
/*后续处理*/
void   free_result ( void )                                         //释放结果集
bool   reset ( void )                                               //重置预处理环境，准备执行新的预处理查询
bool   close ( void )                                               //关闭预处理对象
}
```


常量
=======

* MYSQLI_BOTH  元素以索引和关联两种方式兼有的数组返回
* MYSQLI_NUM  元素以索引数组返回
* MYSQLI_ASSOC 元素以关联数组返回

需要异常处理的方法
===========

* new mysqli() : 创建对象时
* mysqli::query  ：当查询结果超过 max_allowed_packet时，mysql会报错（错误码1153或2006），方法会返回false。
* mysqli::store_result ：如果没有结果集可返回，则返回false;如果读取失败，也会返回false
* mysqli_stmt::execute


meta-data
====

* Property    Description
* name    The name of the column
* orgname    Original column name if an alias was specified
* table    The name of the table this field belongs to (if not calculated)
* orgtable    Original table name if an alias was specified
* def    Reserved for default value, currently always “”
* db    Database (since PHP 5.3.6)
* catalog    The catalog name, always “def” (since PHP 5.3.6)
* max_length    The maximum width of the field for the result set.
* length    The width of the field, as specified in the table definition.
* charsetnr    The character set number for the field.
* flags    An integer representing the bit-flags for the field.
* type    The data type used for this field
* decimals    The number of decimals used (for integer fields) 
