安全问题
------------

问题：如果访问者电脑中了木马，黑客获取到了用户的sessionid，那么黑客就可以伪造sessionid请求网站

解决：当用户首次访问时，将用户的ip和浏览器信息作为判断信息保存到一个session变量；黑客使用伪造的sessionid访问时，把黑客的ip和浏览器信息与判断变量进行比对

```php
session_start();
／／如果判断变量已存在，则进行比较
if(isset($_SESSION['chk']) && $_SESSION['chk']!=$_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']){
    session_unset();
    session_regenerate_id();
    echo "非法用户";
}else{
//如果判断变量不存在，则定义判断变量
    $_SESSION['chk']=$_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT'];
}
```

用法注意
-------

> 在使用session函数时，需要注意一下几个要点：

1. session文件默认保存在/tmp目录下，如果一台服务器有多个应用，任意一个应用触发gc时，其他应用产生的session文件也会纳入gc管理范围
2. session的生命周期，是从最后一次访问时间开始算起，而不是从创建时间算起。
3. session_start()方法调用之前，不能有任何输出，因为它要发送header信息给浏览器。
4. session的所有设置，必须在session_start之前进行。
5. session函数都是以“session_“开头