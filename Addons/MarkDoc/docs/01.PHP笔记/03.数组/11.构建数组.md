 
创建规律性索引数组 
-----------------

<p class="api">
array range ( mixed $start , mixed $end [, number $step = 1 ] )
</p>

指定数值范围或单字节字符范围的起始值和结束值，按可选的步长（默认为1）生成一个这个范围内的字符所构成的有规律的索引数组

这个函数用于快速构建规律性递增元素，结合foreach用于需要有规律的地方，例如表格的列标。

$setp 总是去除符号按正整数对待。如需改变顺序，交换$start和$end即可


填充得到值固定索引数组
-----------------------

<p class="api">
array array_fill ( int $start_index , int $num , mixed $value )
</p>

由 start_index 作为起始索引号，用value作为所有元素的值，构建一个元素个数num为的索引数组。

{y:注意 num 必须是一个大于零的数值，否则 PHP 会发出一条警告。}


填充得到值固定的关联数组
------------------------

<p class="api">
array array_fill_keys ( array $keys , mixed $value )
</p>

使用 $keys 数组中元素的value作为返回数组的key，使用 $value 参数的值作为值，来创建一个新的关联数组。


从已有数组分离出索引数组
------------------------

<p class="api">
array array_keys ( array $input [,mixed $search_value [,bool $strict=false]])
</p>

如果指定了可选参数 search_value，则只返回该值的键名。否则 input 数组中的所有键名都会被返回。

自 PHP 5 起，search_value可以用 strict 参数设为true来进行全等比较（===）
 

<p class="api">
array array_values ( array $input ) 
</p>


切割一个数组得到新数组
-----------------------

<p class="api">
array array_chunk ( array $input , int $size [, bool $preserve_keys=false])
</p>

把数组中的元素按指定数量分割成一个个数组，以这些数组作为元素，返回最终结果数组。

默认返回的数组中所有元素都改变为索引方式。

如果希望保留元素的字符串键名和数值索引号，第三个参数指定为true

!!!!!字符串也有切割函数 <span class="api">array str_split ( string $string [, int $split_length = 1 ] )</span>

