Smarty的核心工作流程为
=======================

> 解析行为设置->conf和等插件设置->载入模板->编译模板->生成缓存

解析设置：
-----------

> smarty定义了大量公共属性来对整个解析过程的各阶段行为进行控制。主要的设置包括：字符集，目录，缓存和编译选项。

载入conf和插件：
-------------

> conf是前后端代码中都可使用的存有变量的配置文件，插件和过滤器是后端提供给前端代码调用的函数，这个流程实际上也可以通过设置属性来完成

> 其他流程都是Smarty内核根据设置自动完成

项目代码基本框架：

```php
<?
//Step 1.设定字符集（utf-8无需设置）
if (function_exists('mb_internal_encoding')) {
  mb_internal_encoding('EUC-CN');
}
define('SMARTY_RESOURCE_CHAR_SET', 'EUC-CN');

//Step 2.创建smarty对象
require_once 'libs/Smarty.class.php';
$smarty = new Smarty();

//Step 3.进行必要的设置
$smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
$smarty->cache_lifetime=3600;

//Step 4.检查缓存分配变量
if(!$smarty->isCached('index.tpl')) {
    //连接数据库，获取数据，分配变量
	$smarty->assign("Name","Fred Irving Johnathan Bradley Peppergill",true);
	$smarty->assign("FirstName",array("John","Mary","James","Henry"));
	$smarty->assign("LastName",array("Doe","Smith","Johnson","Case"));
	$smarty->assign("contacts", array(array("phone" => "1", "fax" => "2", "cell" => "3"),
		array("phone" => "555-4444", "fax" => "555-3333", "cell" => "760-1234")));
}

//Step 5.输出模板

$smarty->display('index.tpl');
```

Smarty中的基本概念
================

> 插件，编译，缓存，目录

插件：
----------

> 是可以在模板通过标签调用的php函数，根据作用和用法，主要分为:函数，块，修改器等类型。

编译：
-------

> 编译文件是介于模板和缓存之间的文件。它是根据模板生成的动态文件，它的输出可以直接发送给浏览器(关闭缓存)也可以生成缓存文件。

> 可以每个模板文件生成一个对应的编译文件，也可以把子模板编译结果合并到最终的主模板编译文件中。

缓存：
------

> 缓存文件由编译文件创建，当存在缓存文件时，smarty直接输出缓存文件内容，而不执行编译文件，从而提高可用性。对于动态页面，可以通过cache_id生成子页面的缓存

目录：
------

每一个项目，至少需要5个目录:

|属性/目录     |作用                                                                                                                                            |
|-----------|------------------------------------------------------------------------------------------------------------------------------------------------|
|templates  |目录存放项目模板文件,通常通常根据主题含有多个子目录，php中将具体的主题的子目录作为项目运行时的模板目录，在运行时，模板目录应只设置一个以提高性能|
|configs    |目录存放项目配置文件,至少应提供一个公共配置文件目录，然后可以直接将配置文件放在主题目录下，使用主题目录作为附加的配置文件目录                   |
|plugins    |目录存放项目自定义的插件，至少应提供smarty的plugins目录                                                                                         |
|templates_c|存放自动生成的的模板编译文件(只需1个)                                                                                                           |
|cache      |目录存放自动生成的缓存文件(只需1个)                                                                                                             |


smarty对象实例会自动将这五个属性设置为 . 目录下的对应目录，通常均需要手动重新设置，否则应保证 . 目录下存在这几个目录

如果smarty作为项目的lib与项目一起发布，建议修改__construct方法中默认目录的设置，并尽可能将配置写入公共属性的默认值

通常，一个网站只需一个编译目录，也可以让同一服务器上的不同网站使用同一个编译目录，使用compile_id来为编译文件做区分。

例如：若所有的模板的compile_dir都是'/path/to/shared_compile_dir'，使用compile_id可以将这些模板的编译文件按域名区分开

```php
$smarty->compile_id = $_SERVER['SERVER_NAME'];
$smarty->compile_dir = '/path/to/shared_compile_dir';
```

缓存
-------

> 后端与缓存有关的设置属性有：`caching，cache_lifetime，cache_dir，cache_id` 。方法主要有: `isCached()，clearAllCache()，clearCache()，display('file.tpl', $cache_id)，assign($var,$value,true)
cache_id：`

当我们访问动态网页时，往往是同一个php文件，通过url中的GET参数来显示不同的内容。对于这样的页面，当dispaly时，为其传入一个参数来区分相同网页的不同内容输出。
当参数对应的内容已经被缓存，则直接输出，如果未缓存，则生成缓存。这个参数就是cache_id，它可以是一个GET变量或多个GET变量的组合

```php
$smarty->display('index.tpl', $_GET['article_id']);
$smarty->display('index.tpl',$_GET['cate_id']. $_GET['article_id']);
```


###缓存组


> 对于动态网站，可能会产生大量的缓存。因此合理组织缓存，不仅有利用管理维护，也有利于提高缓存检索性能

> 组织缓存，可以通过用竖线|来为模板设置缓存保存的目录：display('themes/blue/index.tpl','themes|blue')

`clearCache(null,'a|b|c')` 删除文件 '/a/b/c/*'
clearCache('foo.tpl','a|b|c') 的方式来设置模板名称， 然后Smarty将试图删除'/a/b/c/foo.tpl'。

因为缓存组是“从左到右”顺序删除缓存的，所以你不能单独删除某个缓存如'/a/b/*/foo.tpl'。 你可以另外再设置一个缓存组来达到此目的

###局部禁用缓存

在前端禁用
: 模板变量设置 **nocache**属性，或使用`{nocache}...{/nocache}`,`{dynamic}...{/dynamic}`，insert插件

在后端禁用
: 调用变量分配方法时$nocahce参数设为true；

插件注册时禁用
: registerPlugin() 的第4个参数是$cacheable，其默认是TRUE，设为false。

注意：请确认任何局部禁用缓存的的变量，都可以在缓存文件存在时从PHP获取到值。即这些变量在PHP总应该放在全局的变量分配区域，而不能放在isCached的区块内。

###自定义缓存机制：

> Smarty默认是使用基于文件的缓存机制，自定义缓存实现可以实现类似下面的目的： 用更快的存储引擎或内存来替代较慢的文件系统，或使缓存可以分布到多台缓存服务器上。

通过实现 Smarty_CacheResource_Custom 或 Smarty_CacheResource_KeyValueStore 抽象类来实现自定义缓存机制

Smarty_CacheResource_Custom 是比较简单的API，直接通过覆盖读、写、删除等操作来实现缓存机制。 该API可以使用于任何你觉得适合的方式，或存储到任何你觉得适合的地方。

Smarty_CacheResource_KeyValueStore 的API可让你使用K-V存储模式（比如APC，Memcache等）来实现缓存机制。 更进一步，就算是多层的缓存组如"a|b|c"，该API也让你可以通过删除缓存组"a"来将整个嵌套的缓存组删除， 即使K-V存储机制本身无法实现这种层次结构的存储。

自定义缓存可以放到$plugins_dir目录下并命名为cacheresource.foobarxyz.php， 或者在运行时通过registerCacheResource() 来进行注册。 上面两种方式都必须设置$caching_type 来启动你的自定义缓存机制。

插件
-----------

|插件类型        |解释                                                                                                                                 |
|----------------|-------------------------------------------------------------------------------------------------------------------------------------|
|function        |模板函数，使用{funcition name}{/function}定义，或在插件文件中用php定义                                                               |
|modifier        |变量修改器：对变量的值在输出前进行修饰处理。必须明确return一个值作为最终输出内容                                                     |
|block           |块操作：与修改器相似，但块操作插件是修改被块标签包围的内容。                                                                         |
|compiler        |编译函数仅会在模板的编译过程中调用， 用它在模板嵌入PHP代码内容时比较有用。 如果同时存在编译函数和自定义函数，那么编译函数会优先使用。|
|modifiercompiler|-                                                                                                                                    |
|insert          |插入插件是用来实现模板中{insert}标签的调用，不建议使用。                                                                             |
|postfilter      |Smarty把编译完成后的代码作为第一个参数传递到后置过滤器，接收到其返回经过处理的代码后保存到文件系统。                                 |
|prefilter       |前置过滤器是在模板源码被编译前的时刻执行。常用于删除一些模板中的无用注释、检查注释的来源等等。                                       |
|outputfilter    |执行的时机是在模板载入并执行之后，但在内容显示之前。                                                                                 |
|resource        |-                                                                                                                                    |


###插件的载入

按需自动载入的，当特定的修饰器，函数，资源等被调用的时候插件才会被载入。 而且，一个php进程中只会被载入一次。

###插件定义规则

插件文件命名规则：type.name.php

插件函数名规则：smarty_type_name

如果在php脚本中手动载入插件，可不受命名规则限制。

<p class="api">mixed function smarty_function_name(array $params,Smarty_Internal_Template $template)</p>
函数定义规则及示例

####示例：

```php
<?php
function smarty_function_myPlugin(array $params, Smarty_Internal_Template $template)
{
  // 载入依赖的插件
  $template->smarty->loadPlugin('smarty_shared_make_timestamp');
  // 插件代码
}
```


<p class="api">mixed smarty_modifier_name($value[,$param1,$param2,.....])</p>

修饰器定义规则及示例

修饰器接受模板变量的值$value和1个或多个附加的控制参数，最后return修改后的结果


<p class="api">void smarty_block_name($params, $content,Smarty_Internal_Template $template,&$repeat)</p>
block定义规则

模板中使用语法 `{func} .. {/func}` 调用block插件

!!!!!block插件的name不要与function插件的name相同，因为二者都是以标签调用，会产生冲突(function插件被当作block插件解析)

默认你的block函数将被Smarty调用两次，在开始标签位置调用和在结束标签位置调用。 （参考下面如何通过$repeat来改变这种情况）

从Smarty 3.1开始，开始标签的返回值将会被显示。

块函数只有在开始标签时才有 属性。 模板中传递给块函数的属性都包含在$params参数数组中。 在处理结束标签时，这些属性也是可用的。

$content的值，取决于函数在执行开始标签还是结束标签。 当在开始标签时，它会是null； 当在结束标签时，它会是模板块里面全部的内容。 注意模板块已经是被Smarty处理过的，所以你得到的内容是模板块的输出内容，而不是模板块的源代码。

$repeat是一个引用值，能控制块可以被显示多少次。 当块函数被第一次调用时（开始标签），$repeat默认是true； 随后的调用（结束标签）都是false。 每次当块函数返回时将$repeat设成true， {func}...{/func}间的内容会被再次计算， 计算结果保存$content参数内， 在并且函数将被再次执行


<p class="api">mixed smarty_compiler_name($params,$smarty)</p>

编译函数定义规则：

function smarty_compiler_tplheader($params, Smarty $smarty)
{
    return "<?php\necho '" . $smarty->_current_file . " compiled at " . date('Y-m-d H:M'). "';\n?>";
}

在模板中使用{tplheader}，相当于在编译时插入了一段php代码执行


<p class="api">string smarty_prefilter_name($source,$smarty)</p>

prefilter定义规则：

 第一个参数是模板源码，源码可能已被其他过滤器处理过的。 前置过滤器插件将处理并返回过滤后的源码。 注意这些返回的源码不会被保存，它们仅提供给编译使用。
 
<p class="api">string smarty_postfilter_name($compiled,$smarty)</p>

postfilter定义规则：

 第一个参数是已编译的代码，代码可能被其他过滤器处理过的。 后置过滤器将进行处理并返回处理后的代码。
 
<p class="api">string smarty_outputfilter_name($template_output,$smarty)</p>

outputfilter定义规则：

输出过滤器将处理模板的输出内容

第一个参数是将会被处理的输出内容， 第二个参数是调用该函数的Smarty实例。 插件会对内容进行处理并返回结果。

输出过滤器将处理模板的输出内容， 执行的时机是在模板载入并执行之后，但在内容显示之前。
