BC math扩展
------------

用途：BC math主要用来对{y:字符串表示的任意精度数值}进行数学运算，{y:可以设置返回值的精度}

数学运算时是按参数的原始精度运算，按参数设定的精度返回计算结果；如果进行bccomp运算，则按参数设定的精度进行比较。

除求模函数外，其他bc函数的最后一个参数都是int $scale，设置计算的精度位数

!!!!默认返回值精度为0，即只返回结果的整数部分

!!!!安装： windows版默认内建该扩展，linux需要在编译选项增加--enable-bcmath

<p class="api">bool bcscale(int $scale)</p>
设置所有bc函数返回值精度。

<p class="api">string bcadd(string $left_operand,string $right_operand[,int $scale])</p>
求和

<p class="api">string bcsub(string $left_operand,string $right_operand[,int $scale])</p>
求差

<p class="api">string bcmul(string $left_operand,string $right_operand[,int $scale])</p>
乘法运算

<p class="api">string bcsqrt(string $operand[,int $scale])</p>
求平方根

<p class="api">string bcdiv(string $left_operand,string $right_operand[,int $scale])</p>
除法运算

<p class="api">string bcmod(string $left_operand,string $modulus)</p>
求余运算

<p class="api">string bcpow(string $left_operand,string $right_operand[,int $scale])</p>
幂运算（结果的精度会仅可能少，例如：5^2,$scale=2，返回结果为25，而不是25.00）

<p class="api">int bccomp(string $left_operand,string $right_operand[,int $scale])</p>
比较运算：相等输出0，左大于右输出1，左小于右输出-1

基本数学函数
------------

<p class="api">number abs(mixed $number)</p>

求绝对值，返回整型或浮点型

<p class="api">float round(float $val[,int $precision])</p>

四舍五入取整

<p class="api">float ceil(float $value)</p>

进位取整，{y:注意：返回值仍为浮点型}

<p class="api">float floor(float $value)</p>

舍位取整，{y:注意：返回值仍为浮点型}

<p class="api">float fmod(float $x,float $y)</p>

求余数，{y:注意：返回值仍为浮点型}

<p class="api">mixed max(number $arg1,number $arg2[,...])</p>

返回最大值(如果参数中有数组，则返回数组)

<p class="api">mixed max(array $numbers )</p>

返回数组中的最大值

<p class="api">mixed min(number $arg1,number $arg2[,...])</p>

返回最小值

<p class="api">mixed min(array $numbers )</p>

返回数组中的最小值

<p class="api">int mt_rand([int $min],int $max)</p>

<p class="api">在指定范围内返回随机数</p>

<p class="api">int getrandmax(void)</p>

随机函数返回值的最大可能值 通常同PHP_INT_MAX

<p class="api">bool is_finite(float $val)</p>

判断是否是有理数

<p class="api">bool is_infinite(float $val)</p>

判断是否是无理数

三角函数
------------

> 三角函数返回值都是浮点型

<p class="api">float sin(float $arg)</p>
<p class="api">float cos(float $arg)</p>
<p class="api">float tan(float $arg)</p>
<p class="api">float asin(float $arg)</p>
反正弦
<p class="api">float acos(float $arg)</p>
反余弦
<p class="api">float atan(float $arg)</p>
反正切
<p class="api">float sinh(float $arg)</p>
双曲正弦
<p class="api">float cosh(float $arg)</p>
双曲余弦
<p class="api">float tanh(float $arg)</p>
双曲正切
<p class="api">float atanh(float $arg)</p>
反双曲正切
<p class="api">float hypot(float $x,float $y)</p>
根据直角三角形两条直角边，返回斜边

进制转换
--------------

> 在十进制整形和2、8、16进制字符串之间转换

<p class="api">string decbin(int $number)</p>

<p class="api">十进制数字转为二进制number的字符串</p>

<p class="api">string dechex(int $number)</p>

十进制数字转为16进制number字符串

<p class="api">string decoct(int $number)</p>

十进制数字转为8进制number字符串

<p class="api">number bindec(string $binary_string)</p>

<p class="api">二进制number字符串转为10进制number</p>

<p class="api">number hexdec(string $hex_string)</p>

16进制number字符串转10进制number

<p class="api">number octdec(string $octal_string)</p>

8进制number字符串转10进制number


指数和对数
------------

<p class="api">float exp(float $arg)</p>

计算e的arg次次幂
<p class="api">number pow(number $base,number $exp)</p>

求base的exp次幂
<p class="api">float log10(float $arg)</p>

以10为底数的对数
<p class="api">float log(float $arg[,float $base])</p>

求自然数base为底arg的对数