数据结构
========

SplFixedArray
-------------

> SplFixedArray具有php原生arry的典型功能特征，与php的原生array的不同在于,SplFixedArray数据结构可以限定范围，并进行范围调整。而array是无法限制范围的，所以对于有限元素的数组，使用SplFixedArray可以更快，更安全。
> 有点像array_fill，但是array_fill得到的数组可以增加和减少元素

> {r:unset其中的元素，只是将值赋为null，并不会减少元素。}


####有两种方式创建SplFixedArray实例

1. fromArray静态方法：

	fromArray方法 只接受索引数组，默认会保留原来的key，得到的SplFixedArray的 size = 数组的最大索引号 + 1
	fromArray方法 第二个参数设为false，则不会保留元素原有的key，得到的SplFixedArray的size即为原数组的size

2. 构造方法：

	SplFixedArray对象实例化时按指定的size生成一个实例，{r:实例对象中的每个元素的值都是null，key则是0-base的数字}。

#### 元素访问：

* 使用数组式访问方式更改内部的元素的值，但无法添加新元素，也无法减少元素数量。
* 使用setSize()方法可以修改宽度


```php
SplFixedArray implements Iterator , ArrayAccess , Countable {
/* 方法 */
public __construct ([ int $size = 0 ] ) //参数为fixedarray的元素数量  ,Throws InvalidArgumentException when size is a negative number

public static SplFixedArray fromArray (array $array [,bool $save_indexes=true]) //只接受索引数组！否则抛出InvalidArgumentException
public int   getSize ( void )        //得到fixarray的宽度
public int   setSize ( int $size )   //修改fixarray的宽度
public array toArray ( void )        //导出为原生数组

public int   count ( void )  //得到fixarray的宽度,与getSize()返回值相同

public mixed current ( void )//如果指针位置非法时，调用此方法抛出RuntimeException
public int   key ( void )    //如果指针位置非法时，调用此方法抛出RuntimeException
public void  next ( void )
public void  rewind ( void )
public bool  valid ( void )

public bool  offsetExists ( int $index )
public mixed offsetGet ( int $index ) //如果索引位置非法时，调用此方法抛出RuntimeException
public void  offsetSet ( int $index , mixed $newval )//如果索位为置非法时，调用此方法抛出RuntimeException
public void  offsetUnset ( int $index )//如果索引位置非法时，调用此方法抛出RuntimeException
}
```

```php
$fa = SplFixedArray::fromArray(array(1 => 1, 0 => 2, 3 => 3));
var_dump($fa);
/*
class SplFixedArray#3 (4) {
    int(2)
    int(1)
    NULL
    int(3)
}
*/
```

```php
$fa = SplFixedArray::fromArray(array(1 => 1, 0 => 2, 3 => 3), false);
var_dump($fa);
/*
class SplFixedArray#4 (3) {
    int(1)
    int(2)
    int(3)
}
*/
```



```php
$fa = new SplFixedArray(3);
$fa[1]='a';
//$fa[]='b'; 致命错误，不能添加元素。
//$fa['a']='a'; 致命错误，对象中没有这个元素，对象中的元素是索引为0-2的3个元素
var_dump($fa);
unset($fa[2]);
var_dump($fa);
/*

class SplFixedArray#3 (3) {
    NULL
    NULL
    string(1) "a"
}
class SplFixedArray#3 (3) {
    NULL
    NULL
    NULL
}

*/
```

SplObjectStorage
-----------------

用数组来存放对象实现key与object的映射关系是一个简单有效的办法。

但数组保存对象有两个不足：数组允许重复的元素，数组的键的类型只能是整型或字符串。

SplObjectStorage 则解决了这两个问题：同一个对象只存有一份；映射数据则不受限制。

可以像查询数组元素的value那样，轻松查询到与某个对象映射的data数据。或者使用getInfo方法，获得当前迭代到的对象元素的data数据；

使用current()方法获得当前迭代到的对象。

使用这个数据结构可以方便地将许多对象放在进行有序管理，然后序列化以后保存到数据库。


```php
SplObjectStorage implements Countable , Iterator , Traversable , Serializable , ArrayAccess {
/* 方法 */
public bool   contains ( object $object )                        //判断一个对象是否存在于当前的对象库中
public void   attach ( object $object [, mixed $data = NULL ] )  //向对象库中添加一个对象，并为其设置一个关联的data数据,在数组式访问时$object作为key,$data作为value
public void   detach ( object $object )                          //从对象库中移除一个对象
public void   addAll ( SplObjectStorage $storage )               //把另一个对象库中的对象全部添加到目前的对象库中
public void   removeAll ( SplObjectStorage $storage )            //从当前对象库中移除所有存在于另一对象库中的对象
public void   removeAllExcept ( SplObjectStorage $storage )      //从当前对象库中移除所有在另一个对象库中不存在的对象

public string getHash ( string $object )

public int    count ( void )

public bool   offsetExists ( object $object )
public mixed  offsetGet ( object $object ) //如果对象不存在，抛出UnexpectedValueException
public void   offsetSet ( object $object [, mixed $data = NULL ] )
public void   offsetUnset ( object $object ) //无返回值，也不会抛出异常

public object current ( void )            //返回当前位置的对象
public int    key ( void )
public void   setInfo ( mixed $data )     //设置与当前迭代到的对象关联的data数据(覆盖attach方法的第二个参数设定的值)
public mixed  getInfo ( void )            //返回当前迭代到的对象关联的data数据(attach方法的第二个参数)
public void   next ( void )
public void   rewind ( void )
public bool   valid ( void )

public string serialize ( void )
public void   unserialize ( string $serialized )
}
```




```php
<?php
$o1 = new StdClass;
$o2 = new StdClass;
$s = new SplObjectStorage();
$s->attach($o1); // similar to $s[$o1] = NULL;
$s->attach($o2, "hello"); // similar to $s[$o2] = "hello";

var_dump($s[$o1]);//数组式访问使用了object作为key
var_dump($s[$o2]);

?>
/*
以上例程的输出类似于：
NULL
string(5) "hello"
*/
```

SplHeap
-----------

SPL堆，该类是个抽象方法，唯一需要实现的便是compare方法

{r:注意：遍历SplHeap对象结束后，对象将成为一个空的SplHeap对象，因为每个遍历到的元素都应该是top元素，所以遍历过的元素都会被删除。}

key()方法返回top元素的


```php
abstract SplHeap implements Iterator , Countable {
/* 方法 */

abstract int compare ( mixed $value1 , mixed $value2 )  //Compare elements in order to place them correctly in the heap while sifting up.

__construct ( void )

void   recoverFromCorruption ( void ) //Recover from the corrupted state and allow further actions on the heap

void   insert ( mixed $value )        //向堆中插入数据
bool   isEmpty ( void )   //判断堆是否为空

int    count ( void )   //节点数量

mixed  current ( void ) //当前节点元素，等价于top()方法。如果指针位于无效位置，返回null
mixed  top ( void )     //读取top元素的value，top元素保留。如果指针位于无效位置，返回null
mixed  extract ( void ) //抽出当前top元素。如果指针位于无效位置，抛出RuntimeException
mixed  key ( void )     //实际上等同于count()
void   next ( void )    //top元素变更为下一个节点（原来的top节点无疑将被删除）
void   rewind ( void )  //这个方法实际上没有任何作用，因为指针一直都在堆的top元素
bool   valid ( void )   //检测当前位置是否是一个有效元素
}
```


SplMinHeap
----------

遍历时，最小的元素最先被遍历到。(即top元素最小，离top越远，值越大)


```php
SplMinHeap extends SplHeap implements Iterator , Countable {
/* 方法 */
int compare ( mixed $value1 , mixed $value2 )
}
```

SplMaxHeap
----------

遍历时，最大的元素最先被遍历到。(即top元素最大，离top越远，值越小)


```php
SplMaxHeap extends SplHeap implements Iterator , Countable {
/* 方法 */
int compare ( mixed $value1 , mixed $value2 )
}
```


```php
$obj = new MySimpleHeap();
$obj->insert( 4 );
$obj->insert( 8 );
$obj->insert( 1 );
$obj->insert( 0 );

foreach( $obj as $number ) {
    echo $number."\n";
}

/*
    Output display :
    8
    4
    1
    0
*/
```

SplPriorityQueue
----------------

手动设定队列遍历时的优先级，自定义的heap。同样，{r:指针始终在top元素上，遍历结束后，对象成为空对象。}

该类的insert()方法有两个参数，第一个参数是插入的value，第二个参数$priority用于指定起优先顺序,它的值越大，优先级越高，如果参数中两个元素优先级相同，则先插入的元素最终获得更改的优先级。


```php
SplPriorityQueue implements Iterator , Countable {

const int SplPriorityQueue::EXTR_DATA=1     //遍历得到的元素是insert的$value
const int SplPriorityQueue::EXTR_PRIORITY=2 //遍历得到的元素是insert的$priority
const int SplPriorityQueue::EXTR_BOTH=3     //遍历得到的元素是一个数组array('data'=>$value,'priority'=>$priority)
/* 方法 */
__construct ( void )
int    compare ( mixed $priority1 , mixed $priority2 )
int    count ( void ) //heap中的元素数量

void   setExtractFlags ( int $flags )  //设置top元素的返回形式：默认为只返回元素值，即SplPriorityQueue::EXTR_DATA.

void   insert ( mixed $value , mixed $priority )
bool   isEmpty ( void )  //heap是否为空

mixed  top ( void )      //读取top元素。如果指针位于无效位置，返回null
mixed  current ( void )  //读取top元素,同top()方法。如果指针位于无效位置，返回null
mixed  extract ( void )  //抽出top元素。如果指针位于无效位置，抛出RuntimeException
mixed  key ( void )      //实际上等同于count()
void   next ( void )     //将top指针移动到下一个元素
void   recoverFromCorruption ( void )

void   rewind ( void )//无意义
bool   valid ( void ) //当前位置是否有效
}
```


```php
$obj = new SplPriorityQueue ();
//$obj->setExtractFlags(SplPriorityQueue::EXTR_PRIORITY);
//$obj->setExtractFlags(SplPriorityQueue::EXTR_BOTH);
$obj->insert( 'third' ,1);
$obj->insert( 'furth' ,0);
$obj->insert( 'second',2);
$obj->insert( 'first' ,3);

foreach( $obj as $number ) {
    var_dump( $number);
}
```

SplDoublyLinkedList
-------------------

> 常规的迭代器和数组，在使用foreach遍历时，只能从一个方向遍历。而双向链表数据对象，通过设置模式，可以逆向遍历。

> 虽然你可以用array_reverse()函数反转一个数组进行遍历，但这个过程需要更多时间，同时必须用一个新变量来接受函数的返回值，不仅需要多写几行代码，而且意味着要多一份内存占用。因此在规模比较大的数据情况下，使用SplDoublyLinkedList更方便更有效率。

### 使用方法：

实例化即得到一个空的双向链表，不能以下标方式向其中添加关联元素,只能使用push,unshift方法向链表两端添加节点元素,使用pop,shift从链表两端移除元素。

设置遍历模式，使用foreach进行遍历。

### !!!!!手动使用注意：

如果不使用foreach，而是手动使用双向链表逆向模式时要牢记index不等于key。每次增删元素，都会rebuild元素的key 。

元素增减完成后，如果要手动操作元素，必须首先使用rewind()方法，否则next()和prev()方法不会工作。

对于stack模式，执行过$dl->rewind()以后，index+key=count-1，如果要手动更改元素，则使用$dll[$dll->count()-1-key]的方式来读取或修改指定的key元素。


```php
SplDoublyLinkedList implements Iterator , ArrayAccess , Serializable，Countable {
//迭代方向常量(只能选其一)
coust int  IT_MODE_LIFO=2       //Stack：后进先出（LIFO, Last In First Out） 指针开头是在top元素，反向迭代
coust int  IT_MODE_FIFO=0       //Queue：先进先出（FIFO, First-In-First-Out）指针开头是在bottom元素，正向迭代
//迭代行为常量(只能选其一)
coust int  IT_MODE_DELETE=1     //删除模式:凡是遍历到的元素，都会从链表中删除，FIFO模式下可从链表左端删除连续的元素，LIFO模式可从链表右段删除连续的元素。
coust int  IT_MODE_KEEP=0       //遍历模式

/* 方法 */
__construct ( void )

int   getIteratorMode ( void )
void  setIteratorMode ( int $mode ) //默认为0： SplDoublyLinkedList::IT_MODE_FIFO | SplDoublyLinkedList::IT_MODE_KEEP
bool  isEmpty ( void )              //判断双向链表是否为空

//出栈入栈函数重载方法
mixed shift ( void )   //如果为空，抛出RuntimeException
void  unshift ( mixed $value )
mixed pop ( void )     //如果为空，抛出RuntimeException
void  push ( mixed $value )

int   count ( void )

bool  offsetExists ( int $index )
mixed offsetGet ( int $index )                //当$index不存在或不合法时OutOfRangeException
void  offsetSet ( int $index , mixed $newval )//当$index不存在或不合法时OutOfRangeException
void  offsetUnset ( int $index )              //当$index不存在或不合法时OutOfRangeException

mixed top ( void )     //返回key值最大的元素.如果为空，抛出RuntimeException
void  prev( void )     //移动到上一个index
mixed bottom ( void )  //返回key值最小的元素.如果为空，抛出RuntimeException
mixed key ( void )     //返回当前元素的key
mixed current ( void ) //返回当前元素的值
void  next ( void )    //移动到下一个index
void  rewind ( void )  //更新index后移动到index=0的元素
bool  valid ( void )

public string serialize ( void )
public void unserialize ( string $serialized )
}
```

SplStack栈类
-----------

这个类是SplDoublyLinkedList类的子类，它只是简单的强制设置了Stack遍历模式，所以，你现在只能使用下面两个选项之一

1. SplDoublyLinkedList::IT_MODE_DELETE
1. SplDoublyLinkedList::IT_MODE_KEEP

!!!!!如果使用其他mode常量，抛出RuntimeException

这是对栈的模拟，shift和unshift方法仍可正常使用。

同时，你可以使用类名在其他地方限制参数类型，通过类名实现对数据类型的判断。


```php
SplStack extends SplDoublyLinkedList implements Iterator , ArrayAccess , Countable {
/* 方法 */
__construct ( void )
void setIteratorMode ( int $mode )
}
```


SplQueue类
-------------

这个类是SplDoublyLinkedList类的子类，它只是简单的强制设置了Quene模式，所以，你现在只能使用下面两个选项之一

1. SplDoublyLinkedList::IT_MODE_DELETE
1. SplDoublyLinkedList::IT_MODE_KEEP

!!!!!如果使用其他mode常量，抛出RuntimeException

这是对Queue队列的模拟，所以unshift和pop方法仍可正常使用。


```php
SplQueue extends SplDoublyLinkedList implements Iterator , ArrayAccess , Countable {
/* 方法 */
__construct ( void )
mixed dequeue ( void ) //这是shift方法的别名方法，用来从队列头部移除元素
void  enqueue ( mixed $value ) //这是push方法的别名，向队列尾部添加元素
void  setIteratorMode ( int $mode )
```
