<?php
return array(
	'timeout'=>array(//配置在表单中的键名 ,这个会是config[random]
		'title'=>'间隔时间:',//表单的文字
		'type'=>'select',		 //表单的类型：text、textarea、checkbox、radio、select等
		'options'=>array(		 //select 和radion、checkbox的子选项
			'1'=>'1秒',		 //值=>文字
			'3'=>'3秒',
			'5'=>'5秒',
		),
		'value'=>'1',			 //表单的默认值
	),
);
